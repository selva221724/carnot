from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter , A4
from reportlab.platypus import SimpleDocTemplate,Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet

import csv
import os
from pathlib import Path

import os
import pandas as pd


report_dir = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Report/Report.xlsx'

path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Report/pdf'


out = path

df = pd.read_excel(report_dir)
# print(df)
for region, df_region in df.groupby('Inverter No'):
    df_region.to_csv(out+'/'+region+'.csv',index=False)


csv_path =[]
for i,j,k in os.walk(path):
    for file in k:
        if '.csv' in file:
            csv_path.append(os.path.join(i,file))


def merger(list, output):
    from PyPDF2 import PdfFileMerger
    pdfs = list
    merger = PdfFileMerger()

    for pdf in pdfs:
        merger.append(pdf)

    merger.write(output)
    merger.close()


def csv_to_pdf(input_csv,fileName,title=None,title2=None):

    # Introduction text
    line1 = title
    line2 = title2
    # line3 = '                                            '

    #PDF document layout
    table_style = TableStyle([('ALIGN',(1,1),(-2,-2),'RIGHT'),
                           ('TEXTCOLOR',(1,1),(-2,-2),colors.red),
                           ('VALIGN',(0,0),(0,-1),'CENTER'),
                           ('TEXTCOLOR',(0,0),(0,-1),colors.blue),
                           ('ALIGN',(0,-1),(-1,-1),'CENTER'),
                           ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                           ('TEXTCOLOR',(0,-1),(-1,-1),colors.green),
                           ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                           ('BOX', (0,0), (-1,-1), 0.25, colors.black)
                           ])



    styles = getSampleStyleSheet()
    styleNormal = styles['Normal']
    styleHeading = styles['Heading2']
    styleHeading2 = styles['Heading3']
    styleHeading.alignment = 1
    styleHeading2.alignment = 1 # centre text (TA_CENTRE)

    #Configure style and word wrap
    s = getSampleStyleSheet()
    s = s["BodyText"]
    s.wordWrap = 'CJK'

    # File that must be written to report
    with open (input_csv, 'rt') as csvfile:
        reader = csv.reader(csvfile)
        lista = list(reader)

    conteo = 1
    data2 = [[Paragraph(cell, s) for cell in row] for row in lista]
    t = Table(data2)
    t.setStyle(table_style)
    t._argW[0] = 0.4 * inch
    t._argW[1] = 0.7 * inch
    t._argW[2] = 0.8 * inch

    t._argW[6] = 1 * inch

    t._argW[5] = 1 * inch

    elements = []

    archivo_pdf = SimpleDocTemplate(fileName, pagesize = A4, rightMargin = 30, leftMargin = 30, topMargin = 40, bottomMargin = 28 )

    if line1 is not None:
        elements.append(Paragraph(line1, styleHeading))
    elements.append(Paragraph(line2, styleHeading2))
    elements.append(Spacer(inch, .25*inch))
    elements.append(t)

    archivo_pdf.build(elements)


for i in csv_path:
    name = Path(i).name
    name_pdf = name.replace('_',' ')
    a = "THERMAL IMAGING OF SOLAR PANELS "+name_pdf[:-4]+" – REPORT"
    b = "This report contains critically identified solar module defects on "+name_pdf[:-4]+" area."
    csv_to_pdf(i,out+'/'+name[:-4]+'.pdf',a,b)

for i in csv_path:
    os.remove(i)

#csv_to_pdf(defects_csv,Defects,title2='DEFECTIVE PANELS INVERTER WISE')
#csv_to_pdf(Flight_summary,FS,title= 'THERMAL IMAGING OF SOLAR PANELS - REPORT',title2='FLIGHT SUMMARY')
# csv_to_pdf_image(DF,image_path,title2='Types of defects with examples')


# merger_list = [FS, DF, description_pdf, Defects]
#

# merger(merger_list,out_put + '/' + 'Report_Final.pdf')
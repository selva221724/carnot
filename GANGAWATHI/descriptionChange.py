from pykml import parser
from lxml import etree
import os

defects_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/kml'

project_path = "'http://183.82.33.43/2k20/Gangavathi_22MW"

inverters =[]

for i,j,k in os.walk(defects_path):
    for files in j:
        inverters.append(os.path.join(i,files))


# <html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>Panel Number</td><td> 5U</td></tr><tr><td><span style='font-weight:bold'>Defect Type</td><td> Hotspot</td></tr><tr><td><span style='font-weight:bold'>Criticality</td><td> Minor</td></tr><tr><td><span style='font-weight:bold'>Description</td></tr><tr><td colspan='2'><span style='font-weight:bold'> Stressed/Damaged cell. High reflection on a single celll</td></tr><tr><td></td><td></td></tr><tr><td><span style='font-weight:bold'>Latitude</td><td> 13.7417</td></tr><tr><td><span style='font-weight:bold'>Longitude</td><td> 78.6212</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= 'http://106.51.3.224:6660/2k19/hero_thermal/Cropped_Images/ITC_5/ITC_5_4.png'></td></tr></table></body></html>

# < tr class ='w3-green' > < tr > < td > < span style='font-weight:bold' > String Code < / td > < td > 5.1.1.4.2 < / td > < / tr >

def changer(kml_file,output_file,inv_name):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document

    for place in doc.Placemark:
        temp = place.description
        temp = str(temp)
        latlon = str(place.Point.coordinates)
        latlon = latlon.split(',')

        # b_no = temp.split("Block Name: ")[1].split("\n\nInverter")[0]
        inv_no = temp.split("Inverter No: ")[1].split("\n\nTable No")[0]
        tbno = temp.split("Table No: ")[1].split("\n\nDefect: ")[0]
        defect = temp.split("Defect: ")[1].split("\n\nDescription: ")[0]
        descrip = temp.split("Description: ")[1].split("\n\nModule No")[0]
        mod_no = temp.split("Module No: ")[1].split("\n\nMinimum Temperature(°C): ")[0]
        mini = temp.split("Minimum Temperature(°C): ")[1].split("\n\nMaximum Temperature(°C): ")[0]
        maxi = temp.split("Maximum Temperature(°C): ")[1].split("\n\nImage No:")[0]

        lat = latlon[1]
        long = latlon[0]

        img_no = temp.split("Image No: ")[1]
        # print(img_no)

        if not img_no == "":
            b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>Inverter No:</td><td>" + inv_no + "</td></tr><tr><td><span style='font-weight:bold'>Table No:</td><td>" + tbno + "</td></tr><tr><td><span style='font-weight:bold'>Module No:</td><td>" + mod_no + "</td></tr><tr><td><span style='font-weight:bold'>Defect:</td><td>" + defect + "</td></tr><tr><td><span style='font-weight:bold'>Description:</td><td>" + descrip + "</tr><tr><td><span style='font-weight:bold'>Latitude:</td><td>" + lat + "</td></tr><tr><td><span style='font-weight:bold'>Longitude:</td><td>" + long + "</td></tr><tr><td><span style='font-weight:bold'>Minimum Temperature:</td><td>" + mini + "°C" + "</td></tr><tr><td><span style='font-weight:bold'>Maximum Temperature:</td><td>" + maxi + "°C" + "</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/images/Cropped_Images/" + img_no + ".jpg'></td></tr></table></body></html>"

        else:
            name = latlon[0].replace('.','_')
            b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>Inverter No:</td><td>" + inv_no + "</td></tr><tr><td><span style='font-weight:bold'>Table No:</td><td>" + tbno + "</td></tr><tr><td><span style='font-weight:bold'>Module No:</td><td>" + mod_no + "</td></tr><tr><td><span style='font-weight:bold'>Defect:</td><td>" + defect + "</td></tr><tr><td><span style='font-weight:bold'>Description:</td><td>" + descrip + "</tr><tr><td><span style='font-weight:bold'>Latitude:</td><td>" + lat + "</td></tr><tr><td><span style='font-weight:bold'>Longitude:</td><td>" + long + "</td></tr><tr><td><span style='font-weight:bold'>Minimum Temperature:</td><td>" + mini + "°C" + "</td></tr><tr><td><span style='font-weight:bold'>Maximum Temperature:</td><td>" + maxi + "°C" + "</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/images/"+inv_no+'/' + name + ".jpg'></td></tr></table></body></html>"


        place.description._setText(b)

    with open(output_file, "w") as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    with open(output_file, "ab") as f:
        f.write(etree.tostring(docs, pretty_print=True))



for i in inverters:
    from pathlib import Path

    inv_name = Path(i).name
    inv_name = inv_name.split('.')[0]

    path=[]
    for m,n,o in os.walk(i):
        for file in o:
            path.append(os.path.join(m,file))

    for j in path:
        print(j)
        changer(j,j,inv_name)


from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter
from reportlab.platypus import SimpleDocTemplate,Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet

import csv
import os
from pathlib import Path

# csv_pdf = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Reports_merge/out_csv_pdf'
images_pdf = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Report/merger'
out = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Report'

# csv_path =[]
# for i,j,k in os.walk(csv_pdf):
#     for file in k:
#         csv_path.append(os.path.join(i,file))

images_path =[]
for i,j,k in os.walk(images_pdf):
    for file in k:
        if '.pdf' in file:
            images_path.append(os.path.join(i,file))

from natsort import natsorted

# csv_path = natsorted(csv_path)
images_path = natsorted(images_path)


def merger(list, output):
    from PyPDF2 import PdfFileMerger
    pdfs = list
    merger = PdfFileMerger()

    for pdf in pdfs:
        merger.append(pdf)

    merger.write(output)
    merger.close()


merger_list = []

for i in range(len(images_path)):
    # merger_list.append(csv_path[i])
    merger_list.append(images_path[i])


merger(merger_list,out+'/'+'Report_Final.pdf')
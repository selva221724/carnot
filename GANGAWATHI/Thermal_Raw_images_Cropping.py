import geo
from shapely.geometry import Polygon,Point
from datetime import datetime
start_time = datetime.now()
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path
import numpy as np


cv2.useOptimized()

Tifpath = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Rectified Tiff/merged_thermal.tif'

out_dir = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Raw_images'

defects_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS'


defects_inverter = []
for i,j,k in os.walk(defects_path):
    for file in k:
        if '.kml' in file:
            defects_inverter.append(os.path.join(i,file))

defects_inverter = sorted(defects_inverter)


class Panel:
    def __init__(self,coords,points):
        self.coords = coords
        self.points = points

class Points:
    def __init__(self,coords,tabelid,defect):
        self.coords = coords
        self.tabelid = tabelid
        self.defect = defect

def w2p(x,y):
    x,y = geo.set_crs(x,y,'4326','32643')
    x,y = geo.world2Pixel(geoTrans,x,y)
    return (x,y)



srcImage = gdal.Open(Tifpath)
geoTrans = srcImage.GetGeoTransform()



def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect

def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    # print(M)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


def defects_crop(defectKml):

    inv_name = Path(defectKml).name
    inv_name = inv_name.split('.')[0]

    if not os.path.exists(out_dir+'/'+inv_name):
        os.mkdir(out_dir+'/'+inv_name)

    coords, des = geo.kread(defectKml, kml_type="Point", description=True)

    tabelno = []
    defects = []
    for i in des:
        tbno = i.split("Table No: ")[1].split("\n\nDefect: ")[0]

        tabelno.append(tbno)
        defect = i.split("Defect: ")[1].split("\n\nDescription: ")[0]
        defects.append(defect)

    defectsPts = []
    for (coord,tabelno,defect) in zip(coords,tabelno,defects):
        point = Points(coord,tabelno,defect)
        defectsPts.append(point)

    history = {}
    for pt in defectsPts:
        if not os.path.exists(out_dir + '/' + inv_name + '/' + pt.defect):
            os.mkdir(out_dir + '/' + inv_name + '/' + pt.defect)
        x,y = w2p(pt.coords[0], pt.coords[1])
        p = Point(x,y)
        buf = p.buffer(300, cap_style=3)
        buf = list(buf.exterior.coords)
        xlist = []
        ylist = []
        for j in buf:
            xlist.append(j[0])
            ylist.append(j[1])
        point1 = (int(min(xlist)), int(min(ylist)))
        point2 = (int(max(xlist)), int(max(ylist)))
        try:
            crop_img = tifImg[point1[1] - 10:point2[1] + 10, point1[0] - 10:point2[0] + 10]
            gray = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2GRAY)
            if not cv2.countNonZero(gray) == 0:
                crop_img = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2BGR)
                name = "Table no:"+pt.tabelid+", Defect: "+pt.defect
                if name in history.keys():
                    name = name + "_" +str(history[name])
                    cv2.imwrite(out_dir + '/' + inv_name + '/' + pt.defect + "/" + name + ".jpg", crop_img)
                    history[name] += 1
                else:
                    history[name] = 1
                    cv2.imwrite(out_dir + '/' + inv_name + '/' + pt.defect + "/" + name + ".jpg", crop_img)
        except:
            pass

tifImg = imread(Tifpath)

for i in defects_inverter:
    defects_crop(i)
    print(str(i)+' inverter is done')

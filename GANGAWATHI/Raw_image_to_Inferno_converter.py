import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from collections import Counter
from PIL import ImageColor
from scipy import ndimage
from skimage.transform import rotate
from skimage.io import imread,imsave
from pathlib import Path
import os


path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/rawimage/grayscale/converted_part1'


images = []
for i, j, k in os.walk(path):
    for file in k:
        if file.endswith('.tif' ):
            images.append(os.path.join(i, file))

path_parent = str(Path(path).parent)
path_name = str(Path(path).name)

if not os.path.exists(path_parent+'/'+path_name+'_Inferno'):
    os.mkdir(path_parent+'/'+path_name+'_Inferno')


color = ['#000004', '#02020c', '#050417', '#0a0722', '#10092d', '#160b39', '#1e0c45', '#260c51', '#2f0a5b', '#380962',
         '#400a67', '#490b6a', '#510e6c', '#59106e', '#61136e', '#69166e', '#71196e', '#781c6d', '#801f6c', '#88226a',
         '#902568', '#982766', '#a02a63', '#a82e5f', '#b0315b', '#b73557', '#bf3952', '#c63d4d', '#cc4248', '#d34743',
         '#d94d3d', '#df5337', '#e45a31', '#e9612b', '#ed6925', '#f1711f', '#f47918', '#f78212', '#f98b0b', '#fa9407',
         '#fb9d07', '#fca60c', '#fcb014', '#fbba1f', '#fac42a', '#f8cd37', '#f6d746', '#f4e156', '#f2ea69', '#f2f27d',
         '#f5f992', '#fcffa4']

color_rgb = [ImageColor.getrgb(i) for i in color]

for i in images:
    name = Path(i).name.split('.')[0]

    im = Image.open(i)
    width, height = im.size
    pxl = list(im.getdata())

    cnt = Counter(pxl)
    f = [k for k, v in cnt.items() if v > 100]

    minmum = min(f)
    maximum = max(f)

    length = len(color)

    dif = maximum - minmum
    inter = dif / length

    img = np.array(im)
    cmap = plt.cm.inferno
    norm = plt.Normalize(vmin=minmum, vmax=maximum)
    image = cmap(norm(img))
    plt.imsave(path_parent+'/'+path_name+'_Inferno/'+name+ '.jpg',image)

    img_rot = imread(path_parent+'/'+path_name+'_Inferno/'+name+ '.jpg')
    img_rot = ndimage.rotate(img_rot, -90)
    img_rot = cv2.cvtColor(img_rot, cv2.COLOR_RGBA2BGR)
    cv2.imwrite(path_parent+'/'+path_name+'_Inferno/'+name+ '.jpg', img_rot)
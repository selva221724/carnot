from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from natsort import natsorted
import re
from osgeo import gdal
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
from skimage.io import imread
import os
from matplotlib import pyplot as plt

cv2.useOptimized()
from pathlib import Path
import pandas as pd
from pathlib import Path

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely import geometry

from shapely.geometry import MultiPoint

CAD = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_MOD_CAD'

points = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS'

out = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS_with_modno'

tif =

def CAD_reader(BOUND):
    f = open(BOUND, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except:

            pass

    # ===================================== Load KML and append the Data in variable ============================
    n = 0

    coords = []
    des = []

    for place in doc.Placemark:
        # des = str(place.description)
        # des = des.split('<BR>')[2]
        # des = des.split(' ')[-1]
        # des= des[-1]
        # INVERTER.append(des)
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)

        v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)

        c = []
        for i in range(0, len(v) - 2, 2):
            c.append([float(v[i]), float(v[i + 1])])

        c.append([float(v[-2]), float(v[-1])])
        coords.append(c)

        y = str(place.description)
        des.append(y)

    return coords, des


def files_finder(dir):
    temp = []
    for i, j, k in os.walk(dir):
        for file in k:
            if 'gb.kml' in file:
                pass
            else:
                temp.append(os.path.join(i, file))
    return temp


def read_points(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))
    temp = []
    class_coords = []
    descript = []
    # print('number of features in KML is', len(doc.Placemark))
    coo = []
    for place in doc.Placemark:
        y = str(place.Point.coordinates)
        y = y.split(",")
        y = float(y[0]), float(y[1])
        x = str(place.description)
        class_coords.append(y)
        descript.append(x)

    return class_coords, descript


def KMLgen(features, des, color, KML_name, ):
    kml = simplekml.Kml()
    # r, g, b = color
    for i in range(len(features)):
        pol = kml.newpoint(coords=[features[i]])
        pol.description = des[i]
        # pol.style.polystyle.fill = 1
        # pol.style.polystyle.outline = 1
        # pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
        # pol.style.polystyle.color = simplekml.Color.rgb(r, g, b, 60)

    kml.save(KML_name)

def des_joiner(point_des,cad_des):
    place_index = point_des.find("Image No")
    mod_no = "Module No: "+cad_des+"\n\n"
    hashlist = list(point_des)
    hashlist.insert(place_index,mod_no)
    new_des = ''.join(hashlist)
    return new_des



def points_to_polygon(CAD, points, color, name):
    cad_coords,cad_des = CAD_reader(CAD)
    points_coords, points_des = read_points(points)
    polys = []
    descript = []

    for i in range(len(points_coords)):
        x = points_coords[i]
        p = Point(x)

        for j in range(len(cad_coords)):

            poly = geometry.Polygon([[p[0], p[1]] for p in cad_coords[j]])

            if poly.contains(p):
                polys.append(points_coords[i])
                new_des = des_joiner(points_des[i],cad_des[j])
                descript.append(new_des)


    KMLgen(polys, descript, color, name)


color = {'Open_Circuit': (0, 255, 0), 'Short_Circuit': (0, 0, 255), 'Panel_Failure': (255, 0, 0),
         'By_Pass_Diode_Issues': (255, 255, 0), 'Open_String_Tables': (255, 165, 0),
         'Hotspot': (0, 255, 0), 'Dirt': (0, 0, 255), 'Broken_Glass': (255, 0, 0), 'PID': (255, 255, 0),
         'Diode_Shadow': (255, 255, 0)}

cads = []
for i, j, k in os.walk(CAD):
    for file in k:
        cads.append(os.path.join(i, file))

defects = []
for i, j, k in os.walk(points):
    for file in k:
        defects.append(os.path.join(i, file))

cads = natsorted(cads)
defects = natsorted(defects)

for i in range(len(cads)):
    # defects_inv = files_finder(defects[i])
    inv_name = Path(defects[i]).name[:-4]
    print(inv_name)
    # if not os.path.exists(out + '/' + inv_name):
    #     os.mkdir(out + '/' + inv_name)


    name = Path(defects[i]).name
    color_name = 'Dirt'
    points_to_polygon(cads[i], defects[i], color[color_name], out + '/' + name)

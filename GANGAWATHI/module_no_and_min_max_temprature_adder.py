# ================================== Import Packages ======================================================
import geo
from shapely.geometry import Polygon, Point
from datetime import datetime
import sys
import timeit
import cv2
from PIL import Image
import numpy as np
from pykml import parser
from collections import Counter
from natsort import natsorted
import re
from osgeo import gdal
import simplekml
from pyproj import Proj, transform
import gdal
from skimage.io import imread
import os
from matplotlib import pyplot as plt
cv2.useOptimized()
from pathlib import Path
import pandas as pd
from pathlib import Path
from shapely import geometry
from shapely.geometry import MultiPoint



CAD = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_MOD_CAD'

points = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS'

out = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS_with_modno'

Tifpath = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Rectified Tiff/merged.tif'

filename = gdal.Open(Tifpath)
srcband = filename.GetRasterBand(1)
tifImg = srcband.ReadAsArray()

geoTrans = filename.GetGeoTransform()


def w2p(x, y):
    x, y = geo.set_crs(x, y, '4326', '32643')
    x, y = geo.world2Pixel(geoTrans, x, y)
    return (x, y)


def get_min_max(path):
    # im = Image.open(path)
    # pxl = list(im.getdata())
    path = path.astype('float16')
    f = list(path.flatten())

    cnt = Counter(f)
    lst = [k for k, v in cnt.items() if v > 2]
    # cnt = [i for i in cnt.keys()]
    minmum = min(lst)
    maximum = max(lst)
    return round(minmum, 2), round(maximum, 2)


def CAD_reader(BOUND):
    f = open(BOUND, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except:

            pass

    # ===================================== Load KML and append the Data in variable ============================
    n = 0

    coords = []
    des = []

    for place in doc.Placemark:
        # des = str(place.description)
        # des = des.split('<BR>')[2]
        # des = des.split(' ')[-1]
        # des= des[-1]
        # INVERTER.append(des)
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)

        v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)

        c = []
        for i in range(0, len(v) - 2, 2):
            c.append([float(v[i]), float(v[i + 1])])

        c.append([float(v[-2]), float(v[-1])])
        coords.append(c)

        y = str(place.description)
        des.append(y)

    return coords, des


def files_finder(dir):
    temp = []
    for i, j, k in os.walk(dir):
        for file in k:
            if 'gb.kml' in file:
                pass
            else:
                temp.append(os.path.join(i, file))
    return temp


def read_points(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))
    temp = []
    class_coords = []
    descript = []
    # print('number of features in KML is', len(doc.Placemark))
    coo = []
    for place in doc.Placemark:
        y = str(place.Point.coordinates)
        y = y.split(",")
        y = float(y[0]), float(y[1])
        x = str(place.description)
        class_coords.append(y)
        descript.append(x)

    return class_coords, descript


def KMLgen(features, des, color, KML_name, ):
    kml = simplekml.Kml()
    # r, g, b = color
    for i in range(len(features)):
        pol = kml.newpoint(coords=[features[i]])
        pol.description = des[i]
        # pol.style.polystyle.fill = 1
        # pol.style.polystyle.outline = 1
        # pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
        # pol.style.polystyle.color = simplekml.Color.rgb(r, g, b, 60)

    kml.save(KML_name)

def des_joiner(point_des,cad_des,title ):
    place_index = point_des.find("Image No")
    mod_no = title+cad_des+"\n\n"
    hashlist = list(point_des)
    hashlist.insert(place_index,mod_no)
    new_des = ''.join(hashlist)
    return new_des



def points_to_polygon(CAD, points, color, name):
    cad_coords,cad_des = CAD_reader(CAD)
    points_coords, points_des = read_points(points)
    polys = []
    descript = []

    for i in range(len(points_coords)):
        x = points_coords[i]
        p = Point(x)

        for j in range(len(cad_coords)):

            poly = geometry.Polygon([[p[0], p[1]] for p in cad_coords[j]])

            if poly.contains(p):
                polys.append(points_coords[i])

                point = Point(w2p(points_coords[i][0], points_coords[i][1]))
                buf = point.buffer(25, cap_style=3)
                buf = list(buf.exterior.coords)
                xlist = []
                ylist = []
                for k in buf:
                    xlist.append(k[0])
                    ylist.append(k[1])
                point1 = (int(min(xlist)), int(min(ylist)))
                point2 = (int(max(xlist)), int(max(ylist)))

                try:
                    crop_img = tifImg[point1[1] - 10:point2[1] + 10, point1[0] - 10:point2[0] + 10]

                    minimum, maximum = get_min_max(crop_img)

                    if minimum == -1000 or 0 and maximum == 0 or 0:
                        minimum = None
                        maximum = None
                except:
                    minimum = None
                    maximum = None

                new_des = des_joiner(points_des[i], cad_des[j],title="Module No: ")
                new_des = des_joiner(new_des,str(minimum),title="Minimum Temperature(°C): ")
                new_des = des_joiner(new_des,str(maximum), title="Maximum Temperature(°C): ")
                descript.append(new_des)



    KMLgen(polys, descript, color, name)


color = {'Open_Circuit': (0, 255, 0), 'Short_Circuit': (0, 0, 255), 'Panel_Failure': (255, 0, 0),
         'By_Pass_Diode_Issues': (255, 255, 0), 'Open_String_Tables': (255, 165, 0),
         'Hotspot': (0, 255, 0), 'Dirt': (0, 0, 255), 'Broken_Glass': (255, 0, 0), 'PID': (255, 255, 0),
         'Diode_Shadow': (255, 255, 0)}

cads = []
for i, j, k in os.walk(CAD):
    for file in k:
        cads.append(os.path.join(i, file))

defects = []
for i, j, k in os.walk(points):
    for file in k:
        defects.append(os.path.join(i, file))

cads = natsorted(cads)
defects = natsorted(defects)

for i in range(len(cads)):
    # defects_inv = files_finder(defects[i])
    inv_name = Path(defects[i]).name[:-4]
    print(inv_name)
    # if not os.path.exists(out + '/' + inv_name):
    #     os.mkdir(out + '/' + inv_name)


    name = Path(defects[i]).name
    color_name = 'Dirt'
    points_to_polygon(cads[i], defects[i], color[color_name], out + '/' + name)

from datetime import datetime
from natsort import natsorted
start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from pykml import parser
import os
import pandas as pd

cv2.useOptimized()

defects_kmls_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/Defects/INV_WISE_DEFECTS_with_modno'

output_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/Report'

inverters = []
for i, j, k in os.walk(defects_kmls_path):
    for file in k:
        inverters.append(os.path.join(i, file))

inverters = natsorted(inverters)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass
    des = []
    coords = []
    for place in doc.Placemark:
        x = str(place.Point.coordinates)
        x = x.split(",")[:2]
        coords.append(list(np.float_(x)))

        y = str(place.description)
        des.append(y)

    return coords, des

count =0
source_dict = {}
for i in inverters:
    coords, des = read_points_kml(i)
    for j in range(len(des)):
        print(j)

        # ITC_No = des[j].split("Block Name: ")[1].split("\n\nInverter No")[0]
        inv_no = des[j].split("Inverter No: ")[1].split("\n\nTable No:")[0]
        TEXTSTRING = des[j].split("Table No: ")[1].split("\n\nDefect: ")[0]
        Subclass =des[j].split("Defect: ")[1].split("\n\nDescription: ")[0]
        mod_no = des[j].split("Module No: ")[1].split("\n\nMinimum Temperature(°C): ")[0]
        mini = des[j].split("Minimum Temperature(°C): ")[1].split("\n\nMaximum Temperature(°C): ")[0]
        maxi = des[j].split("Maximum Temperature(°C): ")[1].split("\n\nImage No:")[0]

        Longitude, Latitude = coords[j]
        source_dict.update({count: {
                                    'Inverter No':inv_no,
                                    'Defect': Subclass,
                                    'Table Number': TEXTSTRING,
                                    'Module Number' : mod_no,
                                    'Minimum Temperature(°C)': mini,
                                    'Maximum Temperature(°C)': maxi,
                                    'Latitude': Latitude,
                                    'Longitude': Longitude
        }})
        count+=1

data = pd.DataFrame.from_dict(source_dict)
data = data.T
ind = list(range(len(data)))

ind = [i+1 for i in ind]

data['S.No'] = ind

data = data[['S.No','Inverter No', 'Defect', 'Table Number','Module Number','Minimum Temperature(°C)',
             'Maximum Temperature(°C)', 'Latitude', 'Longitude']]
# data.set_index(pd.Index(ind))

data.to_excel(output_path+'/'+'Report.xlsx', index=False)

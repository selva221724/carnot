from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================

import numpy as np
from osgeo import gdal
import pykml
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path

# inverted wise kmls path incluse robotics, icr..etc
KML_PATH = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/poly_kml'

# b =1

# file = output path
file = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/IT Delivery/GLOBAL'


# ================================== Load .Tiff and KML file ===============================================
def read_kml(kml_file):
    from pathlib import Path
    v = Path(kml_file).name
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except Exception as e:
            print(e)
            return
    # ===================================== Load KML and append the Data in variable ============================
    des = []
    coords = []
    for place in doc.Placemark:

        y = str(place.description)
        des.append(y)

        try:

            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        except:
            try:
                x = str(place.LineString.coordinates)
            except Exception as e:
                print(e)
                return

        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        v = x.split(",")
        try:
            i = 0
            coo = []
            while i < len(v):
                coo.append((v[i], v[i + 1]))
                i += 3
        except:
            pass
        coords.append(coo)
        print(coords)

    print('number of Coordinates Extracted from KML is', len(coords))

    return coords, des


def flatten(l):
    try:
        return flatten(l[0]) + (flatten(l[1:]) if len(l) > 1 else []) if type(l) is list else [l]
    except IndexError:
        return []


def kmlgen(KML_PATH, name, t, r, g, b):
    f = flatten(KML_PATH)[0]
    from pathlib import Path
    u = Path(f).name

    world_coord = []
    world_des = []
    for j in range(len(KML_PATH)):
        try:
            c, d = read_kml(KML_PATH[j])

            for i in range(len(c)):
                coordinates = c[i]
                world_coord.append(coordinates)
                world_des.append(d[i])
        except:
            pass

    l = []
    for i in range(len(world_coord)):
        l.append([world_coord[i]])

    # ================================================ Export As a KML ==================================================

    kml = simplekml.Kml()
    for row in range(len(world_coord)):
        pol = kml.newpolygon(outerboundaryis=world_coord[row])

        pol.style.polystyle.fill = 1
        pol.style.polystyle.outline = 1
        pol.style.linestyle.color = simplekml.Color.rgb(r, g, b)
        pol.style.polystyle.color = simplekml.Color.rgb(r, g, b, 30)
        pol.description = world_des[row]

    from pathlib import Path
    v = Path(KML_PATH[0]).name
    v = v.split('.')
    v = v[0]

    if t == 'one':
        kml.save(str(file) + '/' + v +  '.kml')
    elif t == 'list':
        kml.save(str(file) + '/' + v + '_' + str(name)  + '.kml')


def classwise_splitter(path, classes):
    vertical = []
    for i in range(len(path)):
        result = path[i].find(classes)
        if result != -1:
            vertical.append(path[i])
    return vertical


def size_finder(k):
    size = []
    for i in range(len(k)):
        file = Path() / k[i]  # or Path('./doc.txt')
        si = file.stat().st_size
        si = si / 1000000
        size.append(si)
    s = sum(size)
    return s


def chunks(l, n):
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i + n]


def getsize(g):
    file = Path() / g  # or Path('./doc.txt')
    si = file.stat().st_size
    si = si / 1000000
    return si


def seperator(g):
    s = size_finder(g)
    if s > 2:
        it = 0
        main_list = []
        new = []
        size = 0
        for i in range(len(g)):
            one_size = getsize(g[i])
            size = size + one_size
            if size > 2:
                main_list.append(new)
                size = 0
                new = []
                new.append(g[i])

            else:
                new.append(g[i])
        main_list.append(new)

        t = 'list'
        return main_list, t

    else:
        t = 'one'
        return [g], t

color = {'Open_Circuit': (0, 255, 0), 'Short_Circuit': (0, 0, 255), 'Panel_Failure': (255, 0, 0),
         'By_Pass_Diode_Issues': (255, 255, 0), 'Open_String_Tables': (255, 165, 0),
         'Hotspot': (0, 255, 0), 'Dirt': (0, 0, 255), 'Multicell_Hotspot': (255, 0, 0), 'PID': (255, 255, 0),
         'Diode_Shadow': (255, 255, 0)}

classes = list(color.keys())

paths = []
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        paths.append(os.path.join(root, filename))


h = []
for j in range(len(classes)):
    try:
        path = classwise_splitter(paths, classes[j])
        h.append(path)
        v = Path(path[0]).name
        v = v.split('.')
        v = v[0]
        k = path
        end_path, t = seperator(k)


        for i in range(len(end_path)):
            r,g,b = color[v]
            kmlgen(end_path[i], i + 1, t, r,g,b)

    except Exception as e:
        print('There is no KML for ' + classes[j])

from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================

from pykml import parser
import simplekml
from pyproj import Proj, transform
import numpy as np
import simplekml
import os
from osgeo import gdal, osr
from collections import Counter
import time
import csv
import re
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import ntpath
import pandas as pd

image_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Thermal/Mulkanoor36MW_BlockC_Thermal.tif'
dir = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Delivaries/Block_C/Inverterwise_Cadd/kml'


def get_image_proj(image_path):
    image = gdal.Open(image_path)
    proj = osr.SpatialReference(wkt=image.GetProjection())
    epsg = proj.GetAttrValue('AUTHORITY', 1)
    return epsg


# print(get_image_proj(image_path))
inProjch = Proj(init='epsg:' + get_image_proj(image_path))
# inProjch = Proj(init='epsg:32644')
outProjch = Proj(init='epsg:4326')


def set_crs(x, y):
    return transform(inProjch, outProjch, x, y)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    return (ulX + (x * xDist)), (ulY + (y * yDist))


def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    return (x - ulX) / xDist, (y - ulY) / yDist


def latlon_imgcoord_gen(geotrans, kml_file_path):
    i = 0
    kml_file = kml_file_path
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            print('not able to read')
            return
    try:
        for place in doc.Placemark:
            i += 1
    except:
        return

    polygons = []
    for place in doc.Placemark:
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' not in x:
            x = x.replace(' ', ',')
        else:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
        x = x.strip().split("\n")
        x = [i.split(',') for i in x]
        x = x[0]
        try:
            x.remove('')
        except:
            pass
        i = 0
        coords = []
        try:
            while i < len(x):
                coords.append((x[i], x[i + 1]))
                i += 3
        except:
            pass
        # print(coords)
        new_coord = []

        for coord in coords:
            # print(coord)
            new_coord.append(transform(outProjch, inProjch, coord[0], coord[1]))
        polygons.append(new_coord)
        # print(polygons)
    new_polygon = []
    for polygon in polygons:
        new_coord = []
        for coord in polygon:
            new_coord.append(world2Pixel(geotrans, coord[0], coord[1]))
        new_polygon.append(new_coord)

    # print(new_polygon)
    return new_polygon


def midpoint(p1, p2):
    return Point((p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2)


srcimage = gdal.Open(image_path)
geotrans = srcimage.GetGeoTransform()


# geotrans = (204657.57305, 0.03629, 0.0, 1651985.7490200002, 0.0, -0.03629)


def zangetsu(string, result, path_to_store, name):
    bignew_bound = []
    t = 1 / string
    for i in result:
        # t=0.0161
        newbound = []
        # print(i)

        tx = [a[0] for a in i]
        ty = [a[1] for a in i]

        minx, maxx = min(tx), max(tx)
        miny, maxy = min(ty), max(ty)

        # temp = (minx,miny), (maxx,maxy)  # Left side Top and Bottom Coordinates
        # tmid = midpoint(temp[0],temp[1])  # Finding Mid Point of Left side Top and Bottom Coordinates
        # tempu = (minx,maxy), (tmid.x,tmid.y)  # Left Top to Mid
        # tempd = (tmid.x,tmid.y), (minx,miny)  # Mid To Left Bottom
        # newbound.append([minx,miny,maxx,miny])
        # print(minx,maxx,miny,maxy)
        ip = minx, miny
        for j in range(1, string + 1):
            # dxsplit = minx + (maxx - minx) * t * j  # Finding delta x
            dysplit = miny + (maxy - miny) * t * j
            # ipd = dxsplit,maxy
            # ipd = maxx, dysplit
            # ip = dxsplit,miny
            # ipc = midpoint(ip,ipd)
            # xu = ip,(ipc.x,ipc.y)
            # xd = (ipc.x,ipc.y),ipd
            newbound.append([(ip), (maxx, dysplit)])
            # print(newbound)
            ip = minx, dysplit
        # print(newbound)
        # print(len(newbound))
        # newbound.append([tempd[0], xd[0], xd[1], tempd[1]])
        # tempu=xu
        # tempd=xd
        # midx = midpoint((maxx, maxy),(maxx,miny))
        # xu = (maxx,maxy), (midx.x, midx.y)
        # xd = (midx.x,midx.y), (maxx,miny)
        # newbound.append([tempu[0], xu[0], xu[1], tempu[1]])
        # newbound.append([tempd[0], xd[0], xd[1], tempd[1]])
        bignew_bound.append(newbound)
        # print('reached')
    save_percentage_kml(bignew_bound, geotrans, path_to_store, name)


def save_percentage_kml(bigkml, geotrans, path_to_store, name):
    kml_conv = simplekml.Kml()
    for kml in bigkml:
        desc = 1
        # total = 62
        # print(kml)

        for k in kml:
            # if len(kml)==124:
            #     total=62
            # elif len(kml)==62:
            #     total=31
            # print(len(k))
            # temp = []
            # print(k)
            n, m = Pixel2world(geotrans, k[0][0], k[0][1])
            n, m = set_crs(n, m)
            p1 = n, m
            n, m = Pixel2world(geotrans, k[1][0], k[0][1])
            n, m = set_crs(n, m)
            p2 = n, m
            n, m = Pixel2world(geotrans, k[1][0], k[1][1])
            n, m = set_crs(n, m)
            p3 = n, m
            n, m = Pixel2world(geotrans, k[0][0], k[1][1])
            n, m = set_crs(n, m)
            p4 = n, m
            pol = kml_conv.newpolygon(outerboundaryis=[p1,
                                                       p2,
                                                       p3,
                                                       p4,
                                                       p1])
            pol.style.polystyle.fill = 0
            pol.style.polystyle.outline = 1
            # if desc%2==0:
            #     pol.description='T'+str(int(desc/2))
            #     # pol.description = str(int(desc / 2)+total)
            # else:
            pol.description = str(desc)
            desc += 1
            # print(desc)
    # print('storing', path_to_store + '/split_' + file + '.kml')
    kml_conv.save(path_to_store + '/split_' + file + '.kml')
    # print(kml_conv)


for path, directories, files in os.walk(dir):
    for file in files:
        try:
            kml_path = os.path.join(path, file)
            print(kml_path)
            value = file[:-4]
            result = latlon_imgcoord_gen(geotrans, kml_path)
            # if value == 'small':
            #     string = 21
            if value == 'merged':
                string = 21
            if value == 'large':
                string = 42
            # print(string)
            zangetsu(string, result, path, file[:-4])
        except:
            pass
#
print("Owner: ", u"\u0ba8\u0bb5\u0bc0\u0ba9\u0b82")
print("Assistant: ", u"\u0BB5\u0B9A\u0ba8\u0b82\u0ba4\u0b82")

print(datetime.now())

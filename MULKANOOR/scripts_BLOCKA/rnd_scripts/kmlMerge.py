from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================

import numpy as np
from osgeo import gdal
import pykml
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os


KML_PATH = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/rubbersheet/kml/Rubbersheet/INV_BOUND'


# ================================== Load .Tiff and KML file ===============================================
def read_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document
    print('Number of features in KML is', len(doc.Placemark))

    # ===================================== Load KML and append the Data in variable ============================

    coords = []
    for place in doc.Placemark:
        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        x = x.replace(' ', ',')
        v = x.split(",")
        co = [(v[0], v[1]), (v[3], v[4]), (v[6], v[7]), (v[9], v[10])]
        coords.append(co)

    return coords



path =[]
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        path.append(os.path.join(root,filename))


# =================================== Mergeing All KMLs ====================================================
world_coord =[]
for j in range(len(path)):
    c = read_kml(path[j])
    for i in range(len(c)):
        coordinates = [(c[i][0][0], c[i][0][1]), (c[i][1][0], c[i][1][1]),
                       (c[i][2][0], c[i][2][1]), (c[i][3][0], c[i][3][1])]
        world_coord.append(coordinates)


l = []
for i in range(len(world_coord)):
    l.append([world_coord[i][0][0], world_coord[i][0][1], world_coord[i][1][0], world_coord[i][1][1]
                 ,world_coord[i][2][0], world_coord[i][2][1], world_coord[i][3][0], world_coord[i][3][1]])



# ================================================ Export As a KML ==================================================


kml = simplekml.Kml()
for row in l:
    pol = kml.newpolygon(outerboundaryis=[(row[0], row[1]),
                                                          (row[2], row[3]),
                                                          (row[4], row[5]),
                                                          (row[6], row[7]),
                                                          (row[0], row[1])])



kml.save('merge.kml')

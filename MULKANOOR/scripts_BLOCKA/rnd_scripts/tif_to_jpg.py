import cv2
import os
from skimage.io import imread
from skimage import color
from skimage.transform import rotate

from scipy import ndimage
from skimage.io import imread,imsave

path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Gangavathi_22MW/rawimage/thermal/converted_part6'

images = []
for i, j, k in os.walk(path):
    for file in k:
        if '.tif' in file:
            images.append(os.path.join(i, file))


for i in images:
    img = imread(i)
    img = ndimage.rotate(img, -90)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(i[:-4]+'.jpg',img)




g = input("do you want to delete tifs : y/n")
if g =='y':
    for i in images:
        os.remove(i)
else:
    pass



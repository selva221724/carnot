from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
import os
from pathlib import Path
cv2.useOptimized()

KML_PATH = "/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Defects/BLOCK_A/INV_DEFECTS"
OUT_PUT_PATH = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Defects/BLOCK_A/CLASS_WISE_DEFECTS'

# tif_name = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/SCPM300/Fly06_101019/TIFF/COMPRESSED/COMPRESSED.tif'

# from pathlib import Path
# v = Path(KML_PATH).name
# out = v.replace(".", " ").split()[0]
temp=[]
def KMLgen(feature,des, OUT_PUT_PATH):
    # l = []
    # for i in range(len(feature)):
    #     l.append([feature[i][0][0], feature[i][0][1], feature[i][1][0], feature[i][1][1]
    #                  , feature[i][2][0], feature[i][2][1], feature[i][3][0], feature[i][3][1]])

    # =============================================== Export As a KML ==================================================
    color = {'Open Circuit':'green', 'Short Circuit': 'blue', 'Panel Failure': 'red',
             'By Pass Diode Issues': 'yellow', 'Open String Tables': 'orange',
             'Hotspot': 'green', 'Dirt': 'blue', 'Broken Glass': 'red', 'PID': 'yellow',
             'Diode Shadow':'yellow' }

    # print(feature)
    for ((key,value),(keys,values)) in zip(feature.items(),des.items()):
        kml = simplekml.Kml()
        for (l,k) in zip(value,values) :
            pol = kml.newpoint(coords=[l])

            pol.description = k

            pol.style.labelstyle.color = simplekml.Color.red  # Make the text red
            pol.style.labelstyle.scale = 2  # Make the text twice as big

            pol.style.iconstyle.icon.href = 'http://183.82.33.43/GroupL/colour_icon/'+color[key]+'.png'

            kml.save(OUT_PUT_PATH+'/'+key+".kml")




def readkml(kml_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))
    temp = []
    class_coords = {}
    descript ={}
    # print('number of features in KML is', len(doc.Placemark))
    coo = []
    for place in doc.Placemark:
        y= str(place.Point.coordinates)
        y=y.split(",")
        y = float(y[0]), float(y[1])
        x = str(place.description)

        xx = str(place.description)

        x = x.split("Defect: ")[1].split("\n\nDescription: ")[0]
        # x = x.strip().split("\n")
        # x = [i.strip() for i in x]
        # x = x[4]
        # x = x.split(' ')[-1]
        # for i in x:
        #     i = i.replace(',', '_')
        #     i = i.replace('<BR><B>TEXTSTRING</B>', '')
        #     i = i.split('=')
        #     label = i[2]
        #     temp.append(label)
        #     try:
        #         class_coords[label].append(y)
        #
        #     except KeyError as e:
        #         print(e)
        #         class_coords[label]=[]
        #         class_coords[label].append(y)
        if x not in class_coords.keys():

            class_coords[x] = [y]
            descript[x] = [xx]
        else:
            class_coords[x].append(y)
            descript[x].append(xx)

    print(class_coords.keys())
    return class_coords , descript

path =[]
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        if '.kml' in filename:
            path.append(os.path.join(root,filename))

path = list(sorted(path))

def split(path,out):

        v = Path(path).name
        file = v.replace(".", " ").split()[0]
        print(file)

        # if not os.path.exists(OUT_PUT_PATH+'/'+str(out) + '/'+str(file)):
        #     os.makedirs(OUT_PUT_PATH+'/' + str(out) + '/'+str(file))

        data ,des  = readkml(path)
        # print(data)

        KMLgen(data,des,out)

for i in range(len(path)):
    inv_name = Path(path[i]).name
    # inv_name = inv_name.split('.')[0]

    if not os.path.exists(OUT_PUT_PATH + '/' + inv_name):
        os.makedirs(OUT_PUT_PATH + '/' + inv_name)

    split(path[i],OUT_PUT_PATH + '/' + inv_name)


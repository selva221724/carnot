from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
import os
from pathlib import Path

cv2.useOptimized()

KML_PATH = "/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/IT Delivery/Block_A/kml"
OUT_PUT_PATH = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/IT Delivery/Block_A/GLOBAL'

temp = []


def KMLgen(feature, des, key, no, OUT_PUT_PATH):
    # =============================================== Export As a KML ==================================================
    # color = {'Open_Circuit':'green', 'Short_Circuit': 'blue', 'Panel_Failure': 'red',
    #          'Bypass_Diode_Issues': 'yellow', 'Open_String_Tables': 'orange',
    #          'Hotspot': 'green', 'Dirt': 'blue', 'Broken_Glass': 'red', 'PID': 'yellow'}

    # print(feature)
    kml = simplekml.Kml()
    for (l, k) in zip(feature, des):
        pol = kml.newpoint(coords=[l])

        pol.description = k

        pol.style.labelstyle.color = simplekml.Color.red  # Make the text red
        pol.style.labelstyle.scale = 2  # Make the text twice as big

        pol.style.iconstyle.icon.href = 'http://183.82.33.43/GroupL/colour_icon/' + color[key] + '.png'
        if no == None:
            kml.save(OUT_PUT_PATH + '/' + key + ".kml")
        else:
            kml.save(OUT_PUT_PATH + '/' + key + '_' + str(no + 1) + ".kml")


def readkml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document

    coo = []
    des = []
    for place in doc.Placemark:
        y = str(place.Point.coordinates)
        y = y.split(",")
        y = float(y[0]), float(y[1])
        coo.append(y)

        x = str(place.description)
        des.append(x)

    return coo, des


def size_finder(k):
    size = []
    for i in range(len(k)):
        file = Path() / k[i]  # or Path('./doc.txt')
        si = file.stat().st_size
        si = si / 1000000
        size.append(si)
    s = sum(size)
    return s


def getsize(g):
    file = Path() / g  # or Path('./doc.txt')
    si = file.stat().st_size
    si = si / 1000000
    return si


def seperator(g):
    s = size_finder(g)
    if s > 1.5:
        it = 0
        main_list = []
        new = []
        size = 0
        for i in range(len(g)):
            one_size = getsize(g[i])
            size = size + one_size
            if size > 1.5:
                main_list.append(new)
                size = 0
                new = []
                new.append(g[i])

            else:
                new.append(g[i])
        main_list.append(new)

        t = 'list'
        return main_list, t

    else:
        t = 'one'
        return [g], t


path = []
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        path.append(os.path.join(root, filename))

path = list(sorted(path))

source_dict = {}

color = {'Open_Circuit': 'green', 'Short_Circuit': 'blue', 'Panel_Failure': 'red',
         'By_Pass_Diode_Issues': 'yellow', 'Open_String_Tables': 'orange',
         'Hotspot': 'green', 'Dirt': 'blue', 'Broken_Glass': 'red', 'PID': 'yellow',
         'Diode_Shadow': 'yellow'}

kml_names = list(color.keys())

for i in kml_names:
    temp = []
    for j in path:
        kml_name = Path(j).name
        kml_name = kml_name.split('.')[0]
        if kml_name == i:
            temp.append(j)

    source_dict.update({i: temp})

out = {}
out_des = {}
for (key, value) in source_dict.items():
    temp_data = []
    temp_des = []
    end_path, t = seperator(value)
    if t == "list":
        no = 0
        for i in end_path:
            # print(i,'end path')
            temp_data = []
            temp_des = []
            for q in i:
                # print(q, 'end path')
                data, des = readkml(q)
                for (j, k) in zip(data, des):
                    temp_data.append(j)
                    temp_des.append(k)

            KMLgen(temp_data, temp_des, key, no, OUT_PUT_PATH)
            no += 1

            # out.update({key:temp_data})
            # out_des.update({key:temp_des})
    else:
        no = None
        for q in end_path[0]:
            data, des = readkml(q)
            for (j, k) in zip(data, des):
                temp_data.append(j)
                temp_des.append(k)

        print(key)
        print(temp_data)
        # print(temp_des)
        KMLgen(temp_data, temp_des, key, no, OUT_PUT_PATH)
        # out.update({key: temp_data})
        # out_des.update({key: temp_des})

        # for ((k, v), (i, j)) in zip(out.items(), out_des.items()):
        #     print(k, 'in process')

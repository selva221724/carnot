import geo
from shapely.geometry import Polygon,Point
from datetime import datetime
start_time = datetime.now()
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path
import numpy as np
import pandas as pd


kml = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Delivaries/Block_A/hotspot_ortho/1.kml'

out = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Delivaries/Block_A'



import geopandas as gpd
import fiona
import itertools

fiona.drvsupport.supported_drivers['kml'] = 'rw' # enable KML support which is disabled by default
fiona.drvsupport.supported_drivers['KML'] = 'rw'
g1 = gpd.GeoDataFrame.from_file(kml)
geoms = g1['geometry'].tolist()
intersection_iter = gpd.GeoDataFrame(gpd.GeoSeries([poly[0].intersection(poly[1]) for poly in  itertools.combinations(geoms, 2) if poly[0].intersects(poly[1])]), columns=['geometry'])
union_iter = intersection_iter.unary_union

shapes_series = gpd.GeoSeries([polygon for polygon in union_iter])


for i,j in shapes_series.items():
    coords = list(j.exterior.coords)

    if len(coords) < 5:

        xlist = []
        ylist = []
        for j in coords:
            xlist.append(j[0])
            ylist.append(j[1])
        x1,y1 = min(xlist), min(ylist)
        x2, y2  = max(xlist), max(ylist)
        final_poits = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]
        final_poits = Polygon(final_poits)
        shapes_series.update(pd.Series([final_poits], index=[i]))


shapes_series.to_file(out+'/'+"hotspot_ortho_merged.kml", driver='KML')
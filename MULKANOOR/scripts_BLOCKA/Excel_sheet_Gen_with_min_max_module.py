from datetime import datetime
from natsort import natsorted

import sys
import timeit

start = timeit.default_timer()

# ================================== Import Packages ======================================================
import cv2
from PIL import Image
import numpy as np
from pykml import parser
import os
from collections import Counter
import pandas as pd
from pathlib import Path
from skimage.io import imread, imsave

cv2.useOptimized()

defects_kmls_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Defects/BLOCK_A/INV_DEFECTS_POLY'


output_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/IT Delivery/Block_A/Report'




inverters = []
for i, j, k in os.walk(defects_kmls_path):
    for file in k:
        inverters.append(os.path.join(i, file))

inverters = natsorted(inverters)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass
    des = []
    coords = []
    for place in doc.Placemark:
        # x = str(place.Point.coordinates)
        # x = x.split(",")[:2]
        # coords.append(list(np.float_(x)))

        y = str(place.description)
        des.append(y)

    return des



def get_min_max(path):
    # im = Image.open(path)
    # pxl = list(im.getdata())
    path = path.astype('int32')
    f = list(path.flatten())

    cnt = Counter(f)
    lst = [k for k, v in cnt.items() if v > 100]
    # cnt = [i for i in cnt.keys()]
    minmum = min(lst)
    maximum = max(lst)
    return round(minmum, 2), round(maximum, 2)


count = 0
source_dict = {}
for i in inverters:
    des = read_points_kml(i)
    for j in range(len(des)):
        print(j)
        TEXTSTRING = des[j].split("Table No: ")[1].split("\n\nDefect: ")[0]
        ITC_No = des[j].split("Block Name: ")[1].split("\n\nInverter: ")[0]
        inv_no = des[j].split("Inverter: ")[1].split("\n\nTable No:")[0]
        Subclass = des[j].split("Defect: ")[1].split("\n\nDescription: ")[0]
        mod_no = des[j].split("Module No: ")[1].split("\n\nImage No: ")[0]
        # Longitude, Latitude = coords[j]


        source_dict.update(
            {count: {'Block No': ITC_No, 'Inverter No': inv_no, 'Module No':mod_no, 'Defect': Subclass, 'Table Number': TEXTSTRING}})
        count += 1

data = pd.DataFrame.from_dict(source_dict)
data = data.T
ind = list(range(len(data)))

ind = [i + 1 for i in ind]

data['Serial No'] = ind

data = data[
    ['Serial No', 'Block No', 'Inverter No','Module No','Table Number', 'Defect']]
data.set_index(pd.Index(ind))

data.to_excel(output_path+'/'+'Report2.xlsx', index=False)

#==========================================================================
stop = timeit.default_timer()
total_time = stop - start

# output running time in a nice format.
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)
#
# sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))

import geo
from shapely.geometry import Polygon,Point
from datetime import datetime
start_time = datetime.now()
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path
import numpy as np
import pandas as pd
import geopy.distance
from matplotlib import pyplot as plt
from pyproj import  Proj, transform
import math

kml = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Delivaries/Block_A/inverterwise_cad/Merged_kml.kml'

out = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Delivaries/Block_A/inverterwise_cad/kml'

import geopandas as gpd
import fiona
import itertools

fiona.drvsupport.supported_drivers['kml'] = 'rw' # enable KML support which is disabled by default
fiona.drvsupport.supported_drivers['KML'] = 'rw'
g1 = gpd.read_file(kml)
# g1.crs = {'init': 'epsg:4326'}
g11=g1.to_crs({'init':'epsg:32644'})
g2 = gpd.GeoSeries(g11['geometry'])
# conv=g2.to_crs({'init': 'epsg:32644'})

def distance_cartesian(x1, y1, x2, y2):
    dx = x1 - x2
    dy = y1 - y2

    return math.sqrt(dx * dx + dy * dy)

small =[]
medium =[]
large =[]
leng =[]
for i,j in g2.items():
    coords = list(j.exterior.coords)
    xlist = []
    ylist = []
    for k in coords:
        xlist.append(k[0])
        ylist.append(k[1])
    point1 = min(xlist), min(ylist)
    point2 = max(xlist), max(ylist)
    # length = geopy.distance.vincenty(point1, (point1[1],point2[0])).m
    length =distance_cartesian(point1[0],point1[1],point2[0],point2[1])
    leng.append(length)
    if 0< length < 10:
        small.append(j)
    elif 10 < length < 30:
        medium.append(j)
    elif 30 < length < 80:
        print('large passed')
        large.append(j)




small = gpd.GeoSeries([polygon for polygon in small])
small.crs = {'init': 'epsg:32644'}
small=small.to_crs({'init':'epsg:4326'})
medium = gpd.GeoSeries([polygon for polygon in medium])
medium.crs = {'init': 'epsg:32644'}
medium=medium.to_crs({'init':'epsg:4326'})
large = gpd.GeoSeries([polygon for polygon in large])
large.crs = {'init': 'epsg:32644'}
large=large.to_crs({'init':'epsg:4326'})

small.to_file(out+'/'+"small.kml", driver='KML')
medium.to_file(out+'/'+"medium.kml", driver='KML')
large.to_file(out+'/'+"large.kml", driver='KML')
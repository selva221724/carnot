import fiona
import rasterio
import rasterio.mask
import geopandas as gp
from osgeo import gdal, ogr, osr
import os

# getting image epsg

def get_image_proj(image_path):
    image = gdal.Open(image_path)
    proj = osr.SpatialReference(wkt=image.GetProjection())
    epsg = proj.GetAttrValue('AUTHORITY', 1)
    return epsg

# image clipped using kml

def raster_clip_kml(image_path,kml_path,save_dir):
    epsg=get_image_proj(image_path)
    print(epsg)
    count=0
    for kml_name in os.listdir(kml_path):
        count+=1
        # fiona.drvsupport.supported_drivers['kml'] = 'rw'
        # fiona.drvsupport.supported_drivers['KML'] = 'rw'
        print(kml_name)
        gp.io.file.fiona.drvsupport.supported_drivers['KML'] = 'rw'
        kml=gp.read_file(kml_path+kml_name, driver='KML')
        kml=kml.to_crs(epsg=epsg)
        feature=kml['geometry']
        with rasterio.open(image_path,"r") as src:
            out_image, out_transform = rasterio.mask.mask(src, feature,crop=True)
            out_meta = src.meta.copy()
        out_meta.update({"driver": "GTiff",
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})
        with rasterio.open(save_dir+kml_name[:-4]+".tif", "w", **out_meta) as dest:
            dest.write(out_image)
    print("Total images clipped:", str(count))

# image clipped using shapefile

def raster_clip_shp(image_path,shp_path,save_dir):
    epsg=get_image_proj(image_path)
    count=0
    for shp_name in os.listdir(shp_path):
        count+=1
        if shp_name.endswith('.shp'):
            shp = gp.read_file(shp_path+shp_name)
            driver = ogr.GetDriverByName('ESRI Shapefile')
            dataset = driver.Open(shp_path)
            layer = dataset.GetLayer()
            spatialRef = layer.GetSpatialRef()
            shp_epsg= spatialRef.GetAttrValue("AUTHORITY", 1)
            if epsg == shp_epsg :
                print('No conversion needed')
            else:
                shp = shp.to_crs(epsg=epsg)
                print('Converting shp to imageCRS')
            feature = shp['geometry']
            with rasterio.open(image_path, "r") as src:
                out_image, out_transform = rasterio.mask.mask(src, feature, crop=True)
                out_meta = src.meta.copy()
            out_meta.update({"driver": "GTiff",
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})
            with rasterio.open(save_dir + shp_name[:-4] + ".tif", "w", **out_meta) as dest:
                dest.write(out_image)
    print("Total images clipped:", str(count))


image_path="/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/Thermal_tiff/new/Thermal_hotspot_withbackground.tif"
kml_path="/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/GIS_TO_Automation/CAD/inv_boundary/"
save_dir="/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/Thermal_tiff/inv/"
# shp_path='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/GIS_TO_Automation/CAD/inv_boundary'

raster_clip_kml(image_path,kml_path,save_dir)
# raster_clip_shp(image_path,shp_path,save_dir)





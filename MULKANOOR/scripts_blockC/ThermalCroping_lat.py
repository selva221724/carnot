import geo
from shapely.geometry import Polygon,Point
from datetime import datetime
start_time = datetime.now()
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path
import numpy as np




cv2.useOptimized()

Tifpath = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Thermal/Mulkanoor36MW_BlockC_Thermal.tif'

out_dir = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/IT Delivery/Block_C/Hotspot_Ortho'

defects_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Defects/BLOCK_C/INV_DEFECTS'



defects_inverter = []
for i, j, k in os.walk(defects_path):
    for file in k:
        if '.kml' in file:
            defects_inverter.append(os.path.join(i, file))

defects_inverter = sorted(defects_inverter)


class Panel:
    def __init__(self, coords, points):
        self.coords = coords
        self.points = points


class Points:
    def __init__(self, coords, tabelid, defect):
        self.coords = coords
        self.tabelid = tabelid
        self.defect = defect


def w2p(x, y):
    x, y = geo.set_crs(x, y, '4326', '32644')
    x, y = geo.world2Pixel(geoTrans, x, y)
    return (x, y)


def p2w(x, y):
    x, y = geo.Pixel2world(geoTrans, x, y)
    x, y = geo.set_crs(x, y, '32644', '4326')
    return (x, y)


srcImage = gdal.Open(Tifpath)
geoTrans = srcImage.GetGeoTransform()


def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    # print(M)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


def defects_crop(defectKml):
    cad_name = Path(defectKml).name
    cad_name = cad_name.split('.')[0]

    if not os.path.exists(out_dir + '/' + cad_name):
        os.mkdir(out_dir + '/' + cad_name)

    # polycoords = geo.kread(cad)
    # print(polycoords)

    coords, des = geo.kread(defectKml, kml_type="Point", description=True)
    print(coords)

    lat = []
    for i in coords:
        a = str(i[0])
        a = a.replace('.', '_')
        lat.append(a)

    coords_pixel = [w2p(i[0], i[1]) for i in coords]

    Point_pixel = [Point(i[0], i[1]) for i in coords_pixel]

    out_coords = []

    for i, name in zip(Point_pixel, lat):
        buf = i.buffer(25, cap_style=3)
        buf = list(buf.exterior.coords)
        xlist = []
        ylist = []
        for j in buf:
            xlist.append(j[0])
            ylist.append(j[1])
        point1 = (int(min(xlist)), int(min(ylist)))
        point2 = (int(max(xlist)), int(max(ylist)))
        try:
            crop_img = tifImg[point1[1] - 10:point2[1] + 10, point1[0] - 10:point2[0] + 10]
            gray = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2GRAY)
            if not cv2.countNonZero(gray) == 0:
                crop_img = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2BGR)
                # cv2.imwrite(out_dir + '/' + cad_name + '/'+ name + ".jpg", crop_img)
                out_coords.append([point1, point2])
        except:
            pass

    return out_coords


tifImg = imread(Tifpath)

coord = []
for i in defects_inverter:
    a = defects_crop(i)
    print(i, ' inverter is done')
    coord.append(a)

final_coords = []
for i in coord:
    for j in i:
        try:
            x1, y1 = p2w(j[0][0], j[0][1])
            x2, y2 = p2w(j[1][0], j[1][1])
            final_coords.append([[x1, y1], [x2, y1], [x2, y2], [x1, y2]])
        except:
            pass

geo.kgen(final_coords,out_dir+'/'+'hotspot_ortho.kml')
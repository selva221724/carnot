import cv2
from skimage import io
from skimage.transform import rotate
import PIL
from skimage.io import imshow, show , imsave
import pyexiv2
import fractions
from PIL import Image
from PIL.ExifTags import TAGS
import sys
path='/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/Raw_images/29/QGIS_CONVERTED'


import os
imgs=[]
for i,j,k in os.walk(path):
    for file in k:
       imgs.append(os.path.join(i,file))

from scipy import ndimage
from skimage.io import imread,imsave



for i in imgs:
    img = imread(i)
    img = ndimage.rotate(img, 180)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(i,img)



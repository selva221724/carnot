import pyexiv2
import fractions
import os
from PIL import Image
from PIL.ExifTags import TAGS
import sys
from fractions import Fraction


def decdeg2dms(dd):
    is_positive = dd >= 0
    dd = abs(dd)
    minutes, seconds = divmod(dd * 3600, 60)
    degrees, minutes = divmod(minutes, 60)
    degrees = degrees if is_positive else -degrees
    return (degrees, minutes, seconds)


def float_to_ratio(flt):
    if int(flt) == flt:  # to prevent 3.0 -> 30/10
        return str(int(flt)) + '/' + str(1)
    flt_str = str(flt)
    flt_split = flt_str.split('.')
    numerator = int(''.join(flt_split))
    denominator = 10 ** len(flt_split[1])
    value = str(numerator) + '/' + str(denominator)
    return value


# import re
# lat = '''15°30'46.1642"S'''
#
# def dms_to_dd(lat):
#     deg, minutes, seconds, direction =  re.split('[°\'"]', lat)
#     return (float(deg) + float(minutes)/60 + float(seconds)/(60*60)) * (-1 if direction in ['W', 'S'] else 1)
#

paths = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/Raw_images/24/QGIS_CONVERTED/Mission_1_2'

path = []
for i, j, k in os.walk(paths):
    for file in k:
        path.append(os.path.join(i, file))

from natsort import natsorted

path = natsorted(path)

import pandas as pd

data = pd.read_csv('/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/Raw_images/24/Flir_Mission_1_2.csv')

# path = data['0'].to_list()
lat = data['Latitude'].to_list()
lon = data['Longitude'].to_list()
alt = data['Altitude'].to_list()


def geotagger(path, lat, lon, alt):
    latitude = decdeg2dms(lat)
    longitude = decdeg2dms(lon)

    latitude_str = str(float_to_ratio(latitude[0])) + ' ' + str(float_to_ratio(latitude[1])) + ' ' + str(
        float_to_ratio(float("{0:.2f}".format(latitude[2]))))
    longitude_str = str(float_to_ratio(longitude[0])) + ' ' + str(float_to_ratio(longitude[1])) + ' ' + str(
        float_to_ratio(float("{0:.2f}".format(longitude[2]))))
    altitude_str = str(float_to_ratio(float("{0:.2f}".format(alt))))

    dict = {'EXIF': {'Exif.Image.Make': 'FLIR',
                     'Exif.Image.Model': 'Vue 640 7mm',
                     'Exif.Image.XResolution': '72/1',
                     'Exif.Image.YResolution': '72/1',
                     'Exif.Image.ResolutionUnit': '2',
                     'Exif.Image.Software': '22.20.16.1',
                     'Exif.Image.YCbCrPositioning': '1',
                     'Exif.Image.FNumber': '125/100',
                     'Exif.Image.ExifTag': '232',
                     'Exif.Photo.0x02bc': '60 63 120 112 97 99 107 101 116 32 98 101 103 105 110 61 34 34 32 105 100 61 34 87 53 77 48 77 112 67 101 104 105 72 122 114 101 83 122 78 84 99 122 107 99 57 100 34 63 62 13 10 60 114 100 102 58 82 68 70 32 120 109 108 110 115 58 114 100 102 61 34 104 116 116 112 58 47 47 119 119 119 46 119 51 46 111 114 103 47 49 57 57 57 47 48 50 47 50 50 45 114 100 102 45 115 121 110 116 97 120 45 110 115 35 34 62 13 10 60 114 100 102 58 68 101 115 99 114 105 112 116 105 111 110 32 114 100 102 58 97 98 111 117 116 61 34 34 13 10 120 109 108 110 115 58 67 97 109 101 114 97 61 34 104 116 116 112 58 47 47 112 105 120 52 100 46 99 111 109 47 99 97 109 101 114 97 47 49 46 48 47 34 13 10 120 109 108 110 115 58 70 76 73 82 61 34 104 116 116 112 58 47 47 110 115 46 102 108 105 114 46 99 111 109 47 120 109 112 47 49 46 48 47 34 13 10 60 67 97 109 101 114 97 58 66 97 110 100 78 97 109 101 62 13 10 60 114 100 102 58 83 101 113 62 13 10 60 114 100 102 58 108 105 62 76 87 73 82 60 47 114 100 102 58 108 105 62 13 10 60 47 114 100 102 58 83 101 113 62 13 10 60 47 67 97 109 101 114 97 58 66 97 110 100 78 97 109 101 62 13 10 60 67 97 109 101 114 97 58 67 101 110 116 114 97 108 87 97 118 101 108 101 110 103 116 104 62 13 10 60 114 100 102 58 83 101 113 62 13 10 60 114 100 102 58 108 105 62 49 48 48 48 48 60 47 114 100 102 58 108 105 62 13 10 60 47 114 100 102 58 83 101 113 62 13 10 60 47 67 97 109 101 114 97 58 67 101 110 116 114 97 108 87 97 118 101 108 101 110 103 116 104 62 13 10 60 67 97 109 101 114 97 58 87 97 118 101 108 101 110 103 116 104 70 87 72 77 62 13 10 60 114 100 102 58 83 101 113 62 13 10 60 114 100 102 58 108 105 62 52 53 48 48 60 47 114 100 102 58 108 105 62 13 10 60 47 114 100 102 58 83 101 113 62 13 10 60 47 67 97 109 101 114 97 58 87 97 118 101 108 101 110 103 116 104 70 87 72 77 62 13 10 60 67 97 109 101 114 97 58 84 108 105 110 101 97 114 71 97 105 110 62 48 46 48 48 60 47 67 97 109 101 114 97 58 84 108 105 110 101 97 114 71 97 105 110 62 13 10 60 67 97 109 101 114 97 58 89 97 119 62 45 51 53 57 57 47 49 48 48 60 47 67 97 109 101 114 97 58 89 97 119 62 13 10 60 67 97 109 101 114 97 58 80 105 116 99 104 62 53 47 49 48 48 60 47 67 97 109 101 114 97 58 80 105 116 99 104 62 13 10 60 67 97 109 101 114 97 58 82 111 108 108 62 48 47 49 48 48 60 47 67 97 109 101 114 97 58 82 111 108 108 62 13 10 60 67 97 109 101 114 97 58 71 80 83 88 89 65 99 99 117 114 97 99 121 62 48 46 48 48 60 47 67 97 109 101 114 97 58 71 80 83 88 89 65 99 99 117 114 97 99 121 62 13 10 60 67 97 109 101 114 97 58 71 80 83 90 65 99 99 117 114 97 99 121 62 48 46 48 48 60 47 67 97 109 101 114 97 58 71 80 83 90 65 99 99 117 114 97 99 121 62 13 10 60 67 97 109 101 114 97 58 71 121 114 111 82 97 116 101 62 48 46 48 48 60 47 67 97 109 101 114 97 58 71 121 114 111 82 97 116 101 62 13 10 60 67 97 109 101 114 97 58 68 101 116 101 99 116 111 114 66 105 116 68 101 112 116 104 62 49 54 60 47 67 97 109 101 114 97 58 68 101 116 101 99 116 111 114 66 105 116 68 101 112 116 104 62 13 10 60 70 76 73 82 58 77 65 86 86 101 114 115 105 111 110 73 68 62 48 46 51 46 48 46 48 60 47 70 76 73 82 58 77 65 86 86 101 114 115 105 111 110 73 68 62 13 10 60 70 76 73 82 58 77 65 86 67 111 109 112 111 110 101 110 116 73 68 62 49 48 48 60 47 70 76 73 82 58 77 65 86 67 111 109 112 111 110 101 110 116 73 68 62 13 10 60 70 76 73 82 58 77 65 86 82 101 108 97 116 105 118 101 65 108 116 105 116 117 100 101 62 51 57 54 57 48 47 49 48 48 48 60 47 70 76 73 82 58 77 65 86 82 101 108 97 116 105 118 101 65 108 116 105 116 117 100 101 62 13 10 60 70 76 73 82 58 77 65 86 82 97 116 101 79 102 67 108 105 109 98 82 101 102 62 77 60 47 70 76 73 82 58 77 65 86 82 97 116 101 79 102 67 108 105 109 98 82 101 102 62 13 10 60 70 76 73 82 58 77 65 86 82 97 116 101 79 102 67 108 105 109 98 62 51 48 48 47 49 48 48 48 60 47 70 76 73 82 58 77 65 86 82 97 116 101 79 102 67 108 105 109 98 62 13 10 60 70 76 73 82 58 77 65 86 89 97 119 62 56 56 57 57 47 49 48 48 60 47 70 76 73 82 58 77 65 86 89 97 119 62 13 10 60 70 76 73 82 58 77 65 86 80 105 116 99 104 62 45 56 55 53 47 49 48 48 60 47 70 76 73 82 58 77 65 86 80 105 116 99 104 62 13 10 60 70 76 73 82 58 77 65 86 82 111 108 108 62 45 49 51 56 47 49 48 48 60 47 70 76 73 82 58 77 65 86 82 111 108 108 62 13 10 60 70 76 73 82 58 77 65 86 89 97 119 82 97 116 101 62 57 53 47 49 48 48 60 47 70 76 73 82 58 77 65 86 89 97 119 82 97 116 101 62 13 10 60 70 76 73 82 58 77 65 86 80 105 116 99 104 82 97 116 101 62 50 54 53 47 49 48 48 60 47 70 76 73 82 58 77 65 86 80 105 116 99 104 82 97 116 101 62 13 10 60 70 76 73 82 58 77 65 86 82 111 108 108 82 97 116 101 62 52 48 51 47 49 48 48 60 47 70 76 73 82 58 77 65 86 82 111 108 108 82 97 116 101 62 13 10 60 47 114 100 102 58 68 101 115 99 114 105 112 116 105 111 110 62 13 10 60 47 114 100 102 58 82 68 70 62 13 10 60 63 120 112 97 99 107 101 116 32 101 110 100 61 34 119 34 63 62 0',
                     'Exif.Photo.ExifVersion': '48 50 49 48',
                     'Exif.Photo.ComponentsConfiguration': '1 2 3 0',
                     'Exif.Photo.SubjectArea': '320 256 640 512',
                     'Exif.Photo.FlashpixVersion': '48 49 48 48',
                     'Exif.Photo.ColorSpace': '1',
                     'Exif.Photo.PixelXDimension': '640',
                     'Exif.Photo.PixelYDimension': '512',
                     'Exif.Image.GPSTag': '1786',
                     'Exif.GPSInfo.GPSVersionID': '3 2 0 0',
                     'Exif.GPSInfo.GPSLatitudeRef': 'N',
                     'Exif.GPSInfo.GPSLatitude': latitude_str,
                     'Exif.GPSInfo.GPSLongitudeRef': 'E',
                     'Exif.GPSInfo.GPSLongitude': longitude_str,
                     'Exif.GPSInfo.GPSAltitudeRef': '0',
                     'Exif.GPSInfo.GPSAltitude': altitude_str,
                     'Exif.GPSInfo.GPSTimeStamp': '8/1 1/1 40/1',
                     'Exif.GPSInfo.GPSSpeedRef': 'K',
                     'Exif.GPSInfo.GPSSpeed': '300/1000',
                     'Exif.GPSInfo.GPSTrackRef': 'T',
                     'Exif.GPSInfo.GPSTrack': '8884/100',
                     'Exif.Image.FocalLength': '750/100',
                     'Exif.Thumbnail.Compression': '6',
                     'Exif.Thumbnail.Orientation': '1',
                     'Exif.Thumbnail.XResolution': '72/1',
                     'Exif.Thumbnail.YResolution': '72/1',
                     'Exif.Thumbnail.ResolutionUnit': '2'},
            'IPTC': {},
            'XMP': {}}

    exiv_image = pyexiv2.Image(path)
    exiv_image.modify_all(dict)


for i in range(len(path)):
    geotagger(path[i], lat[i], lon[i], alt[i])

#
# p = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/zambia/R&D/Sample Thermal Video/Cuddapah_sample_thermaal_video/Drone_deploy'
#
# import os
#
# path=[]
# for i,j,k, in os.walk(p):
#     for file in k:
#        path.append(os.path.join(i,file))
#
# for i in path:
#     ex = pyexiv2.Image(i)
#     ex.clear_all()

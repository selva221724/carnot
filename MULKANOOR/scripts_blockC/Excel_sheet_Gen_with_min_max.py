from datetime import datetime
from natsort import natsorted
import geo
from matplotlib import pyplot as plt
from osgeo import gdal
from shapely.geometry import Polygon, Point
from datetime import datetime

import sys
import timeit

start = timeit.default_timer()

# ================================== Import Packages ======================================================
import cv2
from PIL import Image
import numpy as np
from pykml import parser
import os
from collections import Counter
import pandas as pd
from pathlib import Path

cv2.useOptimized()

defects_kmls_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Defects/BLOCK_C/INV_DEFECTS'

output_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/IT Delivery/Block_C/Report'

Tifpath = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/Mulkanoor/Thermal/Mulkanoor36MW_BlockC_Single_band.tif'

Image.MAX_IMAGE_PIXELS = None
# tifImg = imread(Tifpath)

filename = gdal.Open(Tifpath)
srcband = filename.GetRasterBand(1)
tifImg = srcband.ReadAsArray()

geoTrans = filename.GetGeoTransform()

inverters = []
for i, j, k in os.walk(defects_kmls_path):
    for file in k:
        inverters.append(os.path.join(i, file))

inverters = natsorted(inverters)


def w2p(x, y):
    x, y = geo.set_crs(x, y, '4326', '32644')
    x, y = geo.world2Pixel(geoTrans, x, y)
    return (x, y)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass
    des = []
    coords = []
    for place in doc.Placemark:
        x = str(place.Point.coordinates)
        x = x.split(",")[:2]
        coords.append(list(np.float_(x)))

        y = str(place.description)
        des.append(y)

    return coords, des


def get_min_max(path):
    # im = Image.open(path)
    # pxl = list(im.getdata())
    path = path.astype('int32')
    f = list(path.flatten())

    cnt = Counter(f)
    lst = [k for k, v in cnt.items() if v > 2]
    # cnt = [i for i in cnt.keys()]
    minmum = min(lst)
    maximum = max(lst)
    return round(minmum, 2), round(maximum, 2)


count = 0
source_dict = {}
for i in inverters:
    coords, des = read_points_kml(i)
    for j in range(len(des)):
        print(j)
        TEXTSTRING = des[j].split("Table No: ")[1].split("\n\nDefect: ")[0]
        ITC_No = des[j].split("Block Name: ")[1].split("\n\nInverter: ")[0]
        inv_no = des[j].split("Inverter: ")[1].split("\n\nTable No")[0]
        Subclass = des[j].split("Defect: ")[1].split("\n\nDescription: ")[0]
        Longitude, Latitude = coords[j]

        point = Point(w2p(Longitude, Latitude))
        buf = point.buffer(25, cap_style=3)
        buf = list(buf.exterior.coords)
        xlist = []
        ylist = []
        for j in buf:
            xlist.append(j[0])
            ylist.append(j[1])
        point1 = (int(min(xlist)), int(min(ylist)))
        point2 = (int(max(xlist)), int(max(ylist)))

        try:
            crop_img = tifImg[point1[1] - 10:point2[1] + 10, point1[0] - 10:point2[0] + 10]

            minimum, maximum = get_min_max(crop_img)

            if minimum == -1000 or 0 and maximum == 0 or 0:
                minimum = None
                maximum = None
        except:
            minimum = None
            maximum = None

        source_dict.update(
            {count: {'Block No': ITC_No, 'Inverter No': inv_no, 'Defect': Subclass, 'Table Number': TEXTSTRING,
                     'Latitude': Latitude, 'Longitude': Longitude, 'Minimum temperature': minimum,
                     'maximum temperature': maximum}})
        count += 1

data = pd.DataFrame.from_dict(source_dict)
data = data.T
ind = list(range(len(data)))

ind = [i + 1 for i in ind]

data['Serial No'] = ind

data = data[
    ['Serial No', 'Block No', 'Inverter No', 'Defect', 'Table Number', 'Latitude', 'Longitude', 'Minimum temperature',
     'maximum temperature']]
data.set_index(pd.Index(ind))

data.to_excel(output_path + '/' + 'Report.xlsx', index=False)

# ==========================================================================
stop = timeit.default_timer()
total_time = stop - start

# output running time in a nice format.
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)

sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))

import cv2
from skimage.io import imread
import os
import matplotlib.pyplot as plt

path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/IT_Delivery/Images_resized'


images =[]
for i,j,k in os.walk(path):
    for img in k:
        images.append(os.path.join(i,img))

for i in images:
    print(i)
    img = cv2.imread(i)
    rimg = cv2.resize(img, (0,0), fx=0.7, fy=0.7)
    cv2.imwrite(i,rimg)


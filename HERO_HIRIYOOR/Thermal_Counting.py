from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from pykml import parser
import os
import pandas as pd
from natsort import natsorted
cv2.useOptimized()


defects_kmls_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/hiryur/IT_Delivery/kml'
output_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/hiryur/IT_Delivery/Count'

inverter_path = []
inverter_files = []
inverter_names = []
for root, directories, filenames in os.walk(defects_kmls_path):
    for filename in directories:
        inverter_path.append(os.path.join(root, filename))
        inverter_names.append(filename)
        temp = []
        for i, j, k in os.walk(os.path.join(root, filename)):
            for file in k:
                temp.append(os.path.join(i, file))
        inverter_files.append(temp)

inverter_path = natsorted(inverter_path)
inverter_files = natsorted(inverter_files)
inverter_names = natsorted(inverter_names)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass

    coords = []
    for place in doc.Placemark:
        x = str(place.Point.coordinates)
        x = x.split(",")
        coords.append(list(np.float_(x)))
    return coords


import copy
# ========================================== Create the Dict for Counting =============================
Source_Dict = {}


subclass = {"count": 0}

for i in inverter_names:
    Source_Dict.update(copy.deepcopy({i: copy.deepcopy({"Hot_Spot_total": copy.deepcopy(subclass),
                                                        "By_Pass_Diode_Issues": copy.deepcopy(subclass),
                                                        "Dirt / Vegetation": copy.deepcopy(subclass),
                                                        "Broken Glass": copy.deepcopy(subclass),
                                                        "Others": copy.deepcopy(subclass),
                                                        "Short Circuit": copy.deepcopy(subclass),
                                                        "Open Circuit": copy.deepcopy(subclass),
                                                        "Panel Failure_total": copy.deepcopy(subclass),
                                                        "Panel Failure_Module": copy.deepcopy(subclass),
                                                        "Panel Failure_Table": copy.deepcopy(subclass),
                                                        "PID": copy.deepcopy(subclass)
                                                        })}))


#
# # ========================================= Counting the Classes ======================================

for i, j in zip(inverter_files, inverter_names):

    for defects in i:
        if not 'gb.kml' in defects:
            data = read_points_kml(defects)
            Count = len(data)

            if 'Open_Circuit' in defects:
                Source_Dict[j]['Open Circuit']['count'] += Count

            elif 'Short_Circuit' in defects:
                Source_Dict[j]['Short Circuit']['count'] += Count

            elif 'Panel_Failure' in defects:
                Source_Dict[j]['Panel Failure_Module']['count'] += Count

            elif 'By_Pass_Diode_Issues' in defects:
                Source_Dict[j]['By_Pass_Diode_Issues']['count'] += Count

            elif 'Open_String_Tables' in defects:
                Source_Dict[j]['Panel Failure_Table']['count'] += Count

            elif 'Hotspot' in defects:
                Source_Dict[j]['Others']['count'] += Count

            elif 'Dirt' in defects:
                Source_Dict[j]['Dirt / Vegetation']['count'] += Count

            elif 'Broken_Glass' in defects:
                Source_Dict[j]['Broken Glass']['count'] += Count

            elif 'PID' in defects:
                Source_Dict[j]['PID']['count'] += Count

print("Source_Dict",Source_Dict)


# # ======================================== Counting The Total ===========================================
for keys in Source_Dict:
    for key in Source_Dict[keys]:
        Source_Dict[keys]['Hot_Spot_total']['count'] = Source_Dict[keys]['By_Pass_Diode_Issues']['count'] + Source_Dict[keys]['Dirt / Vegetation']['count'] +Source_Dict[keys]['Broken Glass']['count']+Source_Dict[keys]['Others']['count']
        Source_Dict[keys]['Panel Failure_total']['count'] = Source_Dict[keys]['Panel Failure_Module']['count'] + Source_Dict[keys]['Panel Failure_Table']['count']




# # ========================================= Inverter Wise Counting =====================================

defect_type = ['Hotspot','Hotspot','Hotspot','Hotspot','Hotspot','Short Circuit','Open Circuit','Panel Failure',
               'Panel Failure','Panel Failure','PID']

criticality = ['total','By Pass Diode Issues','Dirt / Vegetation','Broken Glass','Others','total','total','total','Module','Table','total']

kml_name = ['Hotspot,Dirt,By_Pass_Diode_Issues,Broken_Glass','By_Pass_Diode_Issues','Dirt','Broken_Glass','Hotspot',
       'Short_Circuit','Open_Circuit','Panel_Failure,Open_String_Tables','Panel_Failure','Open_String_Tables','PID']

colors = ['black','yellow','blue','red','green','blue','green','black','red','orange','yellow']

# subgroub = list(color.keys())
# color_code = list(color.values())


# kml_name = list(color.keys())

length = len(colors)

if not os.path.exists(output_dir+'/'+'IT_Deliver'):
    os.mkdir(output_dir+'/'+'IT_Deliver')


summary=[]
count =0
for inv in inverter_names:

    current_value=[]
    for i in Source_Dict[inv]:
        dictList = list(Source_Dict[inv][i].values())
        current_value.append(dictList)

    from itertools import chain
    current_value = list(chain.from_iterable(current_value))
    summary.append(current_value)

    if count==0:
        df = pd.DataFrame(columns=['block_id', 'subgroup', 'sub_class', 'Count', 'kml_name', 'color_code'], index=range(length))
        df['block_id'] = inv
        df['subgroup'] = defect_type
        df['sub_class'] = criticality
        df['Count'] = current_value
        df['kml_name'] = kml_name
        df['color_code'] = colors
    else:
        dff = pd.DataFrame(columns=['block_id', 'subgroup', 'sub_class', 'Count', 'kml_name', 'color_code'], index=range(length))
        dff['block_id'] = inv
        dff['subgroup'] = defect_type
        dff['sub_class'] = criticality
        dff['Count'] = current_value
        dff['kml_name'] = kml_name
        dff['color_code'] = colors
        df = pd.concat([df, dff])
    count+=1


df.to_csv(output_dir+'/IT_Deliver'+'/'+'Inverter.csv',index=False)


# ============================================= Summary ==========================================================

final_count =[]
for j in range(len(summary[0])):
    temp=[]
    for i in summary:
        temp.append(i[j])
    final_count.append(sum(temp))




df = pd.DataFrame(columns=['defect_type','criticality', 'Count','kml', 'color'], index=range(length))
df['defect_type'] = defect_type
df['criticality'] = criticality
df['Count'] = final_count
df['kml'] = kml_name
df['color'] = colors
df.to_csv(output_dir+'/IT_Deliver'+'/'+'Summary.csv',index=False)
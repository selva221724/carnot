import os
import cv2
# vid_dir='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/zambia/Zambia_Video_data/Zone_4'
vid_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Day_1/CCR_5_MISSION_2/merged.mp4'
out_dir='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Day_1/CCR_5_MISSION_2/frames'

# vid_files=[]
# for i,j,k in os.walk(vid_dir):
#   for file in k:
#     if '.mov' in file:
#       vid_files.append(os.path.join(i,file))
# for m in vid_files:
folder_name=vid_path.split("/")[-1].split(".mp4")[0]
vidcap = cv2.VideoCapture(vid_path)
success,image = vidcap.read()
count = 0
file_no=000000
while success:
  if not os.path.exists(os.path.join(out_dir,folder_name)):
    os.mkdir(os.path.join(out_dir,folder_name))
    print(os.path.join(out_dir,folder_name))
  file_no += 1
  cv2.imwrite(os.path.join(out_dir,folder_name)+"/"+"%d.jpg" % file_no, image)
  count += 25# save frame as JPEG file
  vidcap.set(1, count)
  success,image = vidcap.read()
    # print('Read a new frame: ', success)


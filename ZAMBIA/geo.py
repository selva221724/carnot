from pykml import parser
import re
import numpy as np
import simplekml
from shapely.geometry import Point
import pyproj
from pyproj import Proj, transform
from osgeo import gdal


def kread(path, kml_type='polygon', description=False):
    f = open(path, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except Exception as e:
            print(e)
            return
    if kml_type == 'polygon':
        if description:
            coords = []
            des = []
            for place in doc.Placemark:
                try:
                    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
                    y = str(place.description)
                except:
                    try:
                        x = str(place.LineString.coordinates)
                        y = str(place.description)
                    except Exception as e:
                        print(e)
                        return

                v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)

                c = []
                for i in range(0, len(v) - 2, 2):
                    c.append([v[i], '-'+v[i + 1]])

                des.append(y)
                coords.append(c)

            return coords, des

        else:
            coords = []
            for place in doc.Placemark:
                try:
                    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
                except:
                    try:
                        x = str(place.LineString.coordinates)
                    except Exception as e:
                        print(e)
                        return

                v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)
                c = []
                for i in range(0, len(v) - 2, 2):
                    c.append([v[i], '-'+v[i + 1]])
                coords.append(c)
            return coords

    else:
        if description:
            des = []
            coords = []
            for place in doc.Placemark:
                x = str(place.Point.coordinates)
                x = x.split(",")
                coords.append(list(np.float_(x)))
                y = str(place.description)
                des.append(y)
            return coords, des

        else:
            coords = []
            for place in doc.Placemark:
                x = str(place.Point.coordinates)
                x = x.split(",")[:2]
                coords.append(list(np.float_(x)))
            return coords


def kgen(features, KML_name, kml_type='polygon'):
    if kml_type == 'polygon':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newpolygon(outerboundaryis=[row[0], row[1], row[2], row[3], row[0]])
            # pol.description = des[i]
        kml.save(KML_name)

    elif kml_type == 'line':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newlinestring(coords=[row])
        kml.save(KML_name)

    elif kml_type == 'point':
        kml = simplekml.Kml()
        for row in features:
            pol = kml.newpoint(coords=[row])
        kml.save(KML_name)


def points2box(point, Buffer):
    test = Point(point[0], point[1])
    buf = test.buffer(Buffer, cap_style=3)
    return list(buf.exterior.coords)[:-1]


P = pyproj.Proj(proj='utm', zone=31, ellps='WGS84', preserve_units=True)


def LatLon_To_XY(Lat, Lon):
    return P(Lat, Lon)


def XY_To_LatLon(x, y):
    return P(x, y, inverse=True)


def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def set_crs(x, y, in_projection, out_projection):
    inProj = Proj(init='epsg:' + in_projection)
    outProj = Proj(init='epsg:' + out_projection)
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def get_projection(img_path):
    ds = gdal.Open(img_path)
    prj = ds.GetProjection()
    v = prj.split(',')
    v = v[-1]
    v = v.split('"')
    projection = v[1]
    return projection

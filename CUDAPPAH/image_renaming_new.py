####################################################
# THIS SCRIPT CHANGES NAMES OF CROPPED THERMAL IMAGES
# BASED ON GLOBAL INVERTER-WISE KML
#####################################################

# KML_PATH - GLobal Defects
# images_path - Inverter-wise cropped images
# Change descriptions (Every Time) from line:75

# ================================== Import Packages ======================================================
from pykml import parser
import cv2
import os
from pathlib import Path
import os
cv2.useOptimized()


KML_PATH = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/CAD/INV_DEFECTS"
images_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/IT_delivery/images (copy)'

def readkml(kml_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))
    temp = []
    class_coords = []
    descript =[]
    # print('number of features in KML is', len(doc.Placemark))
    coo = []
    for place in doc.Placemark:
        y= str(place.Point.coordinates)
        y=y.split(",")
        y = float(y[0]), float(y[1])
        class_coords.append(y)
        x = str(place.description)
        descript.append(x)

    return class_coords , descript

class kml_defects:
    def __init__(self,tbno,defect,imgno):
        self.table_no = tbno
        self.defect = defect
        self.img_no = imgno

######################### KML ##############################3

path =[]
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        path.append(os.path.join(root,filename))


################## image ########################

img_path = []
for root, directories, filenames in os.walk(images_path):
    for dir in directories:
        img_path.append(os.path.join(root, dir))

path.sort() # contains sorted kml folder path
img_path.sort() # contains sorted image folder path

# it - count number
for it in range(len(path)):
    coord , des = readkml(path[it])
    tabelno = []
    image_no = []
    defect_list=[]
    for dess in des:
        tbno = dess.split("Table No: ")[1].split("\n\nDefect: ")[0]
        tabelno.append(tbno)
        img_num = dess.split("Image No: ")
        img_num = img_num[-1].split('\n\n')[0]
        image_no.append(img_num)
        defects = dess.split("\n\nDefect: ")[1].split("\n\nDescription:")[0]
        defect_list.append(defects)

    total_list = []
    for tb,im,defec in zip(tabelno,image_no,defect_list):
        total_list.append(kml_defects(tb,defec,im))

    for root, directories, filenames in os.walk(img_path[it]):
        for filename in filenames:
            no = filename.split(".jpg")[0]  # no - image name eg: 21.jpg
            for point in total_list:        # total_list contains defects from kml ( list of kml_defects objects)
                if point.img_no == no:
                    # print("image path:",os.path.join(root,filename))
                    img = cv2.imread(os.path.join(root,filename))
                    deffolder = os.path.join(root,point.defect) # creates folder with defect name
                    if not os.path.exists(deffolder):
                        os.mkdir(deffolder)
                    cv2.imwrite(os.path.join(deffolder,point.img_no + ".jpg"),img)




from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
import re
from osgeo import gdal
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
from skimage.io import imread
import os
from matplotlib import pyplot as plt
cv2.useOptimized()
from pathlib import Path
import pandas as pd
from shapely.geometry import Point



CAD = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/GIS_TO_AUTOMATION/Cadd_Table.kml'

BOUND = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/GIS_TO_AUTOMATION/Merged_Bound.kml'

out = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/GIS_TO_AUTOMATION/CAD_INV'



def KMLgen(features,KML_name,des):

    kml = simplekml.Kml()
    i = 0
    for row in features:
        pol = kml.newpolygon(outerboundaryis=row)
        pol.description = des[i]

        i+=1
    kml.save(KML_name)




f = open(BOUND, "r")
docs = parser.parse(f)
try:
    doc = docs.getroot().Document.Folder

except:
    try:
        doc = docs.getroot().Document
    except:

        pass

# ===================================== Load KML and append the Data in variable ============================
n = 0

coords = []
INVERTER =[]

for place in doc.Placemark:
    des = str(place.description)
    # des = des.split('<BR>')[2]
    des = des.split(' ')[-1]
    # des= des[-1]
    INVERTER.append(des)
    x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)

    v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)

    c = []
    for i in range(0, len(v) - 2, 2):
        c.append([float(v[i]),float(v[i + 1])])

    coords.append(c)

print('number of coordinates',len(coords))
print('number of INVERTERS is',len(INVERTER))

def read_kml(kml_file,string):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except:

            return
    # ===================================== Load KML and append the Data in variable ============================

    coords = []
    des = []
    for place in doc.Placemark:
        if string == 'line':
            try:
                x = str(place.LineString.coordinates)
            except:
                x = str(place.MultiGeometry.LineString.coordinates)
        else:
            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        x = x.split(",")
        if string == 'line':
            co = [(float(x[0]),float(x[1])),(float(x[2]), float(x[3]))]
        else:
            y = str(place.description)
            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
            v = re.findall("\d+\.\d+\d+\d+\d+\d+\d+", x)

            c = []
            for i in range(0, len(v) - 2, 2):
                c.append([float(v[i]), float(v[i + 1])])

            # coords.append(c)
            des.append(y)

        coords.append(c)





    print('number of Coordinates Extracted from KML is',len(coords))

    return coords , des



def list_to_tuple(list):
    k = []
    for g in list:
        k.append(tuple(g))
    return k


CAD_kml , des = read_kml(CAD, 'poly')


from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely import geometry

from shapely.geometry import MultiPoint
# points = MultiPoint(list_to_tuple(CAD_kml[0]))
# centre  = points.centroid

for i in range(len(coords)):
    name = INVERTER[i]
    polys = []
    descript = []

    poly = geometry.Polygon([[p[0], p[1]] for p in coords[i]])

    for j in range(len(CAD_kml)):

        points = MultiPoint(list_to_tuple(CAD_kml[j]))
        p = points.centroid

        if poly.contains(p):
            polys.append(CAD_kml[j])
            descript.append(des[j])

    kml_name = out+'/'+name+'.kml'
    KMLgen(polys,kml_name ,descript)


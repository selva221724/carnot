import os
import pandas as pd


report_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/IT_delivery/Report/Report.xlsx'
out_dir ='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Reports_merge/csv'

df = pd.read_excel(report_dir)
# print(df)
for region, df_region in df.groupby('CCR No'):
    for region1, df_region1 in df_region.groupby('TS No'):
        df_region1.to_csv(out_dir+'/'+region+'_'+region1+'.csv',index=False)
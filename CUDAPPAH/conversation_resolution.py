import cv2
from skimage.io import imread
import os
import matplotlib.pyplot as plt
from pathlib import Path

path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/IT_delivery/images'


images =[]
for i,j,k in os.walk(path):
    for img in k:
        images.append(os.path.join(i,img))

# for i in images:
#     print(i)
#     img = cv2.imread(i)
#     rimg = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
#     cv2.imwrite(i,rimg)


for i in images:
    print(i)
    name = Path(i).name
    pa = str(Path(i).parent)
    img = cv2.imread(i)
    if '.JPG' in name:
        name = name.replace('.JPG', '.jpg')
        os.remove(i)
    cv2.imwrite(pa+'/'+name,img)




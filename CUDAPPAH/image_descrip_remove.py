from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
import os
from pathlib import Path
import os
cv2.useOptimized()


kml_path='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Delivery/IT_delivery/kml/INV_DEFECTS'
OUT_PUT_PATH='/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/CUDDAPA/Delivery/IT_delivery/kml/INV_DEFECTS_WITHOUT_IMGNO'


def KMLgen(feature,des, OUT_PUT):
    # l = []
    # for i in range(len(feature)):
    #     l.append([feature[i][0][0], feature[i][0][1], feature[i][1][0], feature[i][1][1]
    #                  , feature[i][2][0], feature[i][2][1], feature[i][3][0], feature[i][3][1]])

    # =============================================== Export As a KML ==================================================
    kml = simplekml.Kml()
    for i,j in zip(feature,des):

        print(i,j)
        pol = kml.newpoint(coords=[i])
        pol.description = j
    kml.save(OUT_PUT +".kml")

def readkml(kml_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document
    # print('number of features in KML is', len(doc.Placemark))
    temp = []
    class_coords = []
    descript =[]
    # print('number of features in KML is', len(doc.Placemark))
    coo = []
    for place in doc.Placemark:
        y= str(place.Point.coordinates)
        y=y.split(",")
        y = float(y[0]), float(y[1])
        class_coords.append(y)
        x = str(place.description)
        descript.append(x)

    return class_coords , descript

path =[]
for root, directories, filenames in os.walk(kml_path):
    for filename in filenames:
        path.append(os.path.join(root,filename))

for deff in path:
    inv_name = Path(deff).name
    inv_name = inv_name.split('.')[0]
    coord , des = readkml(deff)
    desp_list=[]
    for dess in des:
        img_rem=dess.split("\n\n")
        img_rem.pop(4)
        descrip='\n\n'.join(string for string in img_rem)
        desp_list.append(descrip)

    KMLgen(coord,desp_list,OUT_PUT_PATH+'/'+inv_name)


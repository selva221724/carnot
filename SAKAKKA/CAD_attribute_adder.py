from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from osgeo import gdal
import simplekml
from pyproj import Proj, transform
import gdal
from skimage.io import imread, imsave
from matplotlib import pyplot as plt
import os
import geo
from shapely import geometry
from shapely.geometry import MultiPoint

image_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/TIFF/block 25_transparent_mosaic_group1.tif'
# image = imread(image_path)

main_cad = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/CAD_GEN/FINAL_CAD.kml'


#
# bound_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/CAD_naming/BOUND'
#
# sub_inv_paht = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/CAD_naming/gruop/group.kml'
#
# smb = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/CAD_naming/gruop/group.kml'


# ================================== functions ===============================

def set_crs_in(x, y):
    inProj = Proj(init='epsg:4326', preserve_units=True)
    outProj = Proj(init='epsg:32637', preserve_units=True)
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def set_crs(x, y):
    inProj = Proj(init='epsg:32637')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def pixel_coords_conv(coords, geoTrans):
    world_coord = []
    for i in coords:
        temp = []
        for coord in i:
            x, y = set_crs_in(float(coord[0]), float(coord[1]))
            x, y = world2Pixel(geoTrans, x, y)
            temp.append([x, y])
        world_coord.append(temp)

    return world_coord


def KMLgen(KML_name, world_coord, des):
    kml = simplekml.Kml()
    i = 0
    for row in world_coord:
        pol = kml.newpolygon(outerboundaryis=[row[0], row[1], row[2], row[3], row[0]])
        pol.description = des[i]
        pol.style.polystyle.fill = 1
        pol.style.polystyle.outline = 1
        pol.style.linestyle.color = simplekml.Color.rgb(0, 255, 0)
        pol.style.polystyle.color = simplekml.Color.rgb(0, 255, 0, 30)
        i += 1
    kml.save(KML_name)


def list_to_tuple(list):
    k = []
    for g in list:
        k.append(tuple(g))
    return k


# =========================================== Reading Kmls ==================================================

srcImage = gdal.Open(image_path)
geoTrans = srcImage.GetGeoTransform()

# ======================================= CAD ===========================================
CAD = geo.kread(main_cad)
pixel_cad = pixel_coords_conv(CAD, geoTrans)


from shapely.geometry import Polygon


poly = [Polygon(i) for i in pixel_cad]
centroid = [list(Polygon(i).centroid.coords) for i in poly]
centroid_updated = [[int(i[0][0]),int(i[0][1])] for i in centroid]


from operator import itemgetter
#
indices, L_sorted = zip(*sorted(enumerate(centroid_updated), key=lambda k: [k[0], k[1]]))

# indices, L_sorted = zip(*sorted(enumerate(centroid_updated), key=lambda k: k[0]))


mylist = [pixel_cad[i] for i in indices]



world_coord = []
desc = []
count = 1
for i in mylist:
    temp=[]
    coords = i
    for coord in coords:
        x, y = Pixel2world(geoTrans, coord[0], coord[1])
        x, y = set_crs(x, y)
        temp.append([x, y])
    world_coord.append(temp)
    desc.append('B_'+str(count))
    count += 1

KMLgen("/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/CAD_GEN/sorted.kml", world_coord, desc)

# # ==================================== End of Code  ===============================================================
#
# end_time = datetime.now()
# print('======================================================================================')
# print('Total Duration of Over All Execution is {}'.format(end_time - start_time), "seconds")

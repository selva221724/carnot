import os
import cv2
from scipy import ndimage
from skimage.io import imread, imsave

paths = [
         '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/raw_data/09_02_2020',
         '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/raw_data/12_02_2020']

for path in paths:

    pths = []
    for i, j, k in os.walk(path):
        for file in k:
            if '.mov' in file:
                pths.append(os.path.join(i, file))

    for row in pths:

        print(row + ' is processing')

        vid_path = row

        from pathlib import Path

        out_dir = str(Path(vid_path).parent)
        name = Path(vid_path).name
        name = name[:-4]

        if not os.path.exists(out_dir + '/images'):
            os.mkdir(out_dir + '/images')

        vidcap = cv2.VideoCapture(vid_path)
        success, image = vidcap.read()
        count = 0
        file_no = 000000
        while success:
            file_no += 1

            cv2.imwrite(out_dir + '/images/' + "%d.jpg" % file_no, image)

            img = imread(out_dir + '/images/' + "%d.jpg" % file_no)
            img = ndimage.rotate(img, -90)
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            cv2.imwrite(out_dir + '/images/' + "%d.jpg" % file_no, img)

            count += 25  # save frame as JPEG file
            vidcap.set(1, count)
            success, image = vidcap.read()

        print(row + ' is finished')

from datetime import datetime
from natsort import natsorted
start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from pykml import parser
import os
import pandas as pd

cv2.useOptimized()

defects_kmls_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/defects/INV_WISE_DEFECTS'

output_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery/Report'

inverters = []
for i, j, k in os.walk(defects_kmls_path):
    for file in k:
        inverters.append(os.path.join(i, file))

inverters = natsorted(inverters)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass
    des = []
    coords = []
    for place in doc.Placemark:
        x = str(place.Point.coordinates)
        x = x.split(",")[:2]
        coords.append(list(np.float_(x)))

        y = str(place.description)
        des.append(y)

    return coords, des

count =0
source_dict = {}
for i in inverters:
    coords, des = read_points_kml(i)
    for j in range(len(des)):
        print(j)
        TEXTSTRING = des[j].split("Table No: ")[1].split("\n\nDefect: ")[0]
        ITC_No = des[j].split("LVPR No: ")[1].split("\n\nTable No")[0]
        # inv_no = des[j].split("Inverter No: (")[1].split(")\n\nTable No:")[0]
        Subclass =des[j].split("Defect: ")[1].split("\n\nDescription: ")[0]
        Longitude, Latitude = coords[j]
        source_dict.update({count: {'LVPR No': ITC_No,'Table No':TEXTSTRING, 'Defect': Subclass,
                                'Latitude': Latitude, 'Longitude': Longitude}})
        count+=1

data = pd.DataFrame.from_dict(source_dict)
data = data.T
ind = list(range(len(data)))

ind = [i+1 for i in ind]

data['Serial No'] = ind

data = data[['Serial No','LVPR No','Table No', 'Defect', 'Latitude', 'Longitude']]
# data.set_index(pd.Index(ind))

data.to_excel(output_path+'/'+'Report.xlsx', index=False)

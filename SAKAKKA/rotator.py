import cv2
from skimage import io
from skimage.transform import rotate
import PIL
from skimage.io import imshow, show , imsave
import fractions
from PIL import Image
import sys
path='/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery/images (copy)'


import os
imgs=[]
for i,j,k in os.walk(path):
    for file in k:
       imgs.append(os.path.join(i,file))

from scipy import ndimage
from skimage.io import imread,imsave



for i in imgs:
    img = imread(i)
    img = ndimage.rotate(img, 90)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(i,img)



from PIL import Image

import os

path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/Deliveries/imgs'

imgs =[]
for i,j,k in os.walk(path):
    for file in k:
        imgs.append(os.path.join(i,file))

from pathlib import Path
for i in imgs:

    image1 = Image.open(i)
    im1 = image1.convert('RGB')

    im1.save(i[:-4]+'.pdf')

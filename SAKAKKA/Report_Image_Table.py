from pykml import parser
from reportlab.lib import colors
from reportlab.lib.pagesizes import inch, letter
from reportlab.platypus import SimpleDocTemplate,Paragraph, Spacer, Table, TableStyle, Image
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.enums import TA_CENTER
import os

defects_path = "/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery/kml"
imagepath = "/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery"
outpath = "/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery/Report/img/"

missing_data = []
class defects:
    def __init__(self):
        self.moduleno = None
        self.tableno = None
        self.defect = None
        self.imagepath = None
        self.cellno = None

def readkml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document
    total_defects = []
    for place in doc.Placemark:
        defect = defects()
        temp = place.description
        temp = str(temp)
        # tablenotmp = temp.split("Table No:</td><td>")[1].split("</td></tr>")[0]
        defect.tableno = temp.split("Table No:</td><td>")[1].split("</td></tr>")[0]
        # defect.tableno = tablenotmp.split("/")[1]
        # defect.moduleno = tablenotmp.split("/")[0]
        defect.defect = temp.split("Defect:</td><td>")[1].split("</td></tr>")[0]

        img_path = temp.split("/2k20/")[1].split("'></td>")[0]
        img_path = img_path.split('/')[1:]
        string =''
        for i in img_path:
            string = string+'/'+i

        defect.imagepath = string
        # defect.cellno = temp.split("Module No:</td><td>")[1].split("</td></tr>")[0]
        total_defects.append(defect)
    return total_defects

def csv_to_pdf_image(kml_path,fileName):

   #PDF document layout
   table_style = TableStyle([('ALIGN',(0,0),(-1,-1),'CENTER'),
                          ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                          ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                             ('FONTNAME', (0,0), (-1,0), 'Helvetica-Bold')])

   styles = getSampleStyleSheet()
   styleNormal = styles['Normal']
   styleHeading = styles['Heading2']
   styleHeading.alignment = TA_CENTER
   styleHeading2 = styles['Heading2']

   #Configure style and word wrap
   s = getSampleStyleSheet()
   s = s["BodyText"]
   s.wordWrap = 'CJK'

   path=[]
   for m,n,o in os.walk(kml_path):
       for file in o:
           path.append(os.path.join(m,file))
   try:
       totalData = []
       for i in path:
            total_defects = readkml(i)
            try:

                data2 = [[Image(imagepath+defect.imagepath,width=2*inch, height=0.5*inch),defect.tableno,defect.defect] for defect in total_defects]
                for j in data2:
                    totalData.append(j)
            except Exception as e:
                missing_data.append(e)

       columnname = ["Thermal image","Table No","Defect"]

       totalData.insert(0,columnname)
       t = Table(totalData)
       t.setStyle(table_style)

       elements = []

       archivo_pdf = SimpleDocTemplate(outpath+fileName+".pdf", pagesize = letter, rightMargin = 40, leftMargin = 40, topMargin = 40, bottomMargin = 28)

       elements.append(Paragraph("THERMAL DEFECTS AND THEIR IMAGES", styleHeading))
       elements.append(Paragraph(fileName, styleHeading2))
       # elements.append(Spacer(inch, .25*inch))
       elements.append(t)

       archivo_pdf.build(elements)

   except Exception as e:
       print(e)

inverters = []
for i,j,k in os.walk(defects_path):
    for files in j:
        inverters.append(os.path.join(i,files))

for i in inverters:
    from pathlib import Path
    inv_name = Path(i).name
    csv_to_pdf_image(i,inv_name)

print("Mission data ("+str(len(missing_data))+"): ",missing_data)
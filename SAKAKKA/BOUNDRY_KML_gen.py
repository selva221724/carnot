# ================================== Import Packages ======================================================

import numpy as np
from osgeo import gdal
import pykml
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path

# GB KML FILE
kml_file = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/defects/INV_WISE_BOUND'

OUT_PUT_PATH = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/SAKAKA/IT_Delivery/GB_KML'

kmls = []
for i,j,k in os.walk(kml_file):
    for file in k:
        kmls.append(os.path.join(i,file))


for kml in kmls:

    v = Path(kml).name
    file = v.replace(".", " ").split()[0]

    # ================================== Read KML file ===============================================
    if not os.path.exists(OUT_PUT_PATH+'/'+file):
        os.makedirs(OUT_PUT_PATH+'/'+file)

    f = open(kml, "r")
    docs = parser.parse(f)
    try:

        doc = docs.getroot().Document
        print('Number of features in KML is', len(doc.Placemark))
    except:
        try:
            doc = docs.getroot().Document.Folder
            print('Number of features in KML is', len(doc.Placemark))
        except:
            pass
    # ===================================== Global Splitting ============================

    coords =[]
    for place in doc.Placemark:

        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        v = x.split(",")


        line = []
        for i in range(len(v)):
            try:
                if float(v[i]) > 1:
                    line.append(float(v[i]))
            except:
                pass

        cnt = []
        for i in range(0, len(line), 2):
            cnt.append((line[i], line[i + 1]))

        coords.append(cnt)
    kml = simplekml.Kml()

    for feature in coords:
        pol = kml.newpolygon(outerboundaryis=feature)
        pol.style.polystyle.fill = 0
        pol.style.polystyle.outline = 1
        pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 0)

    kml.save(OUT_PUT_PATH + '/' + file + '/' + 'gb.kml')



    # kml = simplekml.Kml()
    # for i in range(len(coords)):
    #     pol = kml.newpolygon(outerboundaryis=coords[i])
    #     pol.style.polystyle.fill = 1
    #     pol.style.polystyle.color = simplekml.Color.rgb(245, 245, 245)
    #     pol.style.polystyle.outline = 1
    #     pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 0)
    #
    # for i in range(len(fill)):
    #     pol = kml.newpolygon(outerboundaryis=fill[i])
    #     pol.style.polystyle.fill = 0
    #     pol.style.polystyle.outline = 1
    #     pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 0)

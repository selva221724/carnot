from datetime import datetime
start_time = datetime.now()


import math
import matplotlib.tri
import matplotlib.pyplot as plt
import random
import cv2
import numpy as np
from osgeo import gdal
import pykml
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path
from scipy.spatial import ConvexHull


Target_kml = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/rubbersheet/target.kml'

Source_CAD_file = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/rubbersheet/kml/SWB_boundary.kml'


kml = Target_kml
points_kml = Source_CAD_file

def map(p):
    triangle = trifinder(p[0], p[1])

    if triangle == -1:
        # No triangle found : don't change the point
        print('not chnaged')
        return p[0], p[1]
    else:
        # Triangle found : adapt it from the old mesh to the new mesh
        a1 = pointsA[delaunay.triangles[triangle][0]]
        a2 = pointsA[delaunay.triangles[triangle][1]]
        a3 = pointsA[delaunay.triangles[triangle][2]]

        b1 = pointsB[delaunay.triangles[triangle][0]]
        b2 = pointsB[delaunay.triangles[triangle][1]]
        b3 = pointsB[delaunay.triangles[triangle][2]]

        mappedP = mapPointFromTriangleAtoTriangleB(p, a1, a2, a3, b1, b2, b3)

        return int(mappedP[0]),int(mappedP[1])

def mapPointFromTriangleAtoTriangleB(p, a1, a2, a3, b1, b2, b3):
    cT = fromCartesianToTriangular(p, a1, a2, a3)
    cC = fromTriangularToCartesian(cT, b1, b2, b3)
    return cC

def fromCartesianToTriangular(p, t1, t2, t3):
    """ Returns triangular coordinates (l1, l2, l3) for a given point in a given triangle """
    """ p is a duplet for cartesian coordinates coordinates """
    x, y = p
    x1, y1 = t1[0], t1[1]
    x2, y2 = t2[0], t2[1]
    x3, y3 = t3[0], t3[1]
    l1 = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3))
    l2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3))
    l3 = 1 - l1 - l2
    return (l1, l2, l3)

def fromTriangularToCartesian(l, t1, t2, t3):
    """ l is a triplet for barycentric coordinates """
    x = l[0] * t1[0] + l[1] * t2[0] + l[2] * t3[0]
    y = l[0] * t1[1] + l[1] * t2[1] + l[2] * t3[1]
    return (x, y)

# Check if a point is inside a rectangle
def rect_contains(rect, point):
    if point[0] < rect[0]:
        return False
    elif point[1] < rect[1]:
        return False
    elif point[0] > rect[2]:
        return False
    elif point[1] > rect[3]:
        return False
    return True


# Draw a point
def draw_point(img, p, color):
    cv2.circle(img, p, 2, color,1,8,0)


# Draw delaunay triangles
def draw_delaunay(img, subdiv, delaunay_color):
    triangleList = subdiv.getTriangleList()
    size = img.shape
    r = (0, 0, size[1], size[0])

    for t in triangleList:

        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])

        if rect_contains(r, pt1) and rect_contains(r, pt2) and rect_contains(r, pt3):
            cv2.line(img, pt1, pt2, delaunay_color, 1, 8, 0)
            cv2.line(img, pt2, pt3, delaunay_color, 1, 8, 0)
            cv2.line(img, pt3, pt1, delaunay_color, 1, 8, 0)






def read_kml(kml_file,string):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        try:
            doc = docs.getroot().Document
        except:

            return
    # ===================================== Load KML and append the Data in variable ============================

    coords = []
    des = []
    for place in doc.Placemark:

        if string == 'line' or  string == 'target':
            try:
                x = str(place.LineString.coordinates)
            except:
                x = str(place.MultiGeometry.LineString.coordinates)
        else:
            x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        x = x.split(",")
        if string == 'line':
            try:
                co = [[float(x[0]),float(x[1])],[float(x[3]), float(x[4])]]
                string = 'line'
            except:
                pass

        if string == 'target':
            try:
                co = [[float(x[0]), float(x[1])], [float(x[2]), float(x[3])]]
            except:
                pass

        else:
            try:
                i = 0
                co = []
                while i < len(x):
                    co.append([float(x[i]), float(x[i + 1])])
                    i += 3
                string = 'poly'
            except:
                pass

        try:
            h = str(place.description)
            # print(h)
            des.append(h)
        except:
            pass

        coords.append(co)





    print('number of Coordinates Extracted from KML is',len(coords))

    return coords , des,string

def convex_hull(point,coeff):

    hull_points = np.array(point)
    hull = ConvexHull(hull_points)
    # for simplex in hull.simplices:
    #     plt.plot(hull_points[simplex, 0], hull_points[simplex, 1], 'k-')
    stretchCoef = coeff
    pointsStretched = hull_points[hull.vertices]*stretchCoef
    # pointsStretched = pointsStretched.tolist()
    # pointsStretched = np.array(pointsStretched,dtype=int)

    pointsStretched_1 = []
    for i in pointsStretched:
        pointsStretched_1.append((i[0],i[1]))

    return pointsStretched_1

d , desd,string_type = read_kml(kml,'target')

pointA = []
pointB = []
for i in d:
    pointA.append(i[0])
    pointB.append(i[1])

# print(pointA)

import pyproj
import math

P = pyproj.Proj(proj='utm', zone=31, ellps='WGS84', preserve_units=True)
G = pyproj.Geod(ellps='WGS84')
def LatLon_To_XY(Lat,Lon):
    return P(Lat,Lon)


def XY_To_LatLon(x,y):
    return P(x,y,inverse=True)


pointA_global = []

for i in pointA:
    x, y = LatLon_To_XY(i[0],i[1])
    pointA_global.append((int(x*1000000000),int(y*1000000000)))


pointB_global = []
for j in pointB:
    x, y = LatLon_To_XY(j[0],j[1])
    pointB_global.append((int(x*1000000000),int(y*1000000000)))




points ,dedd,string_type = read_kml(points_kml,'poly')

points_global = []
for j in points:
    d = []
    for i in range(len(j)):
        x, y = LatLon_To_XY(j[i][0], j[i][1])
        d.append((int(x*1000000000),int(y*1000000000)))
    points_global.append(tuple(d))

# pointsA = convex_hull(pointA_global,0.01)
# pointsB = convex_hull(pointB_global,0.01)


# pointA_global = convex_hull(pointA_global,1.2)
# pointB_global = convex_hull(pointB_global,1.2)

newla = [e for sl in points_global for e in sl]
hull_points = np.array(newla)
hull = ConvexHull(hull_points)
suma = hull_points[hull.vertices]*1.2
hullk = ConvexHull(suma)
# ==================================================================
# conv = hull_points[hull.vertices]
cx = np.mean(hull_points[hull.vertices,0])
cy = np.mean(hull_points[hull.vertices,1])


from osgeo import ogr

point = ogr.Geometry(ogr.wkbPoint)
point.AddPoint(cx, cy)
bufferDistance = 20000000000000
poly = point.Buffer(bufferDistance)
wkt = poly.ExportToWkt()
wkt = wkt[10:]
wkt = wkt[:-2]
wkt = wkt.split(',')
buffer_points =[]
for i in range(len(wkt)):
    h = wkt[i].split(' ')
    buffer_points.append([float(h[0]),float(h[1])])

buffer_points = np.array(buffer_points)

plt.plot(hull_points[:,0], hull_points[:,1], 'o')
plt.plot(buffer_points[:,0], buffer_points[:,1], 'o')
for simplex in hull.simplices:
    plt.plot(hull_points[simplex, 0], hull_points[simplex, 1], 'k-')

# for simplex in hullk.simplices:
#     plt.plot(suma[simplex, 0], suma[simplex, 1], 'k-')
# ================================================================================
# suma = hull_points[hull.vertices]

for i in buffer_points:
    pointA_global.append(i)
    pointB_global.append(i)


pointsA = pointA_global
pointsB = pointB_global



# delaunay = matplotlib.tri.Triangulation([p[0] for p in pointA_global],[p[1] for p in pointA_global])
delaunay = matplotlib.tri.Triangulation([p[0] for p in pointsA],[p[1] for p in pointsA])

trifinder = delaunay.get_trifinder()


mapped_points_global =[]

for j in points_global:
    d = []
    for i in range(len(j)):
        x, y = map((j[i][0], j[i][1]))
        d.append(((x / 1000000000, y /1000000000)))
    mapped_points_global.append(tuple(d))

mapped_points =[]

for j in mapped_points_global:
    d = []
    for i in range(len(j)):
        x, y = XY_To_LatLon(j[i][0], j[i][1])
        d.append(((x , y)))
    mapped_points.append(tuple(d))

def KMLgen(features,KML_name,string):
    i=0
    if string =='line':
        kml = simplekml.Kml()
        for row in range(len(features)):
            z = list(features[row])
            line = kml.newlinestring(coords=z)
            line.description = des[i]
            i += 1
    elif string =='poly':
        kml = simplekml.Kml()
        for row in range(len(features)):
            z = list(features[row])
            pol = kml.newpolygon(outerboundaryis=z)

            # pol.description = des[i]
            i += 1


    kml.save(KML_name)

KMLgen(mapped_points,'/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/rubbersheet/cad_out.kml',string_type)



end_time = datetime.now()
print('Total Duration of the Execution is {}'.format(end_time - start_time), "seconds")



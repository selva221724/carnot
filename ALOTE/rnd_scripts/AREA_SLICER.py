import cv2
import numpy as np
from matplotlib import pyplot as plt



def nothing(x):
  pass
cv2.namedWindow('Takvaviya')


cv2.createTrackbar("R1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("G1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("B1", "Takvaviya",0,255,nothing)
cv2.createTrackbar("R2", "Takvaviya",255,255,nothing)
cv2.createTrackbar("G2", "Takvaviya",255,255,nothing)
cv2.createTrackbar("B2", "Takvaviya",255,255,nothing)
cv2.createTrackbar("Area", "Takvaviya",0,1,nothing)

from skimage.io import imread
img = imread('/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/tiff/test_rdb_alote.tif')


rimg = cv2.resize(img, (0,0), fx=0.4, fy=0.4)



while(1):
   R2 =255

   cv =0
   R1=cv2.getTrackbarPos("R1", "Takvaviya")
   G1=cv2.getTrackbarPos("G1", "Takvaviya")
   B1 = cv2.getTrackbarPos("B1", "Takvaviya")
   R2 = cv2.getTrackbarPos("R2", "Takvaviya")
   G2 = cv2.getTrackbarPos("G2", "Takvaviya")
   B2 = cv2.getTrackbarPos("B2", "Takvaviya")
   Area = cv2.getTrackbarPos("Area", "Takvaviya")
   hsv = cv2.cvtColor(rimg, cv2.COLOR_RGB2HSV)
   mask = cv2.inRange(hsv, (R1, G1, B1), (R2, G2, B2))
   imask = mask > 0
   green = np.zeros_like(rimg, np.uint8)
   green[imask] = rimg[imask]
   final = cv2.cvtColor(green, cv2.COLOR_RGB2BGR)


   if Area ==1 :
      hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
      mask = cv2.inRange(hsv, (R1, G1, B1), (R2, G2, B2))
      imask = mask > 0
      green = np.zeros_like(img, np.uint8)
      green[imask] = img[imask]
      gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
      ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
      contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
      area=[]
      for i in contours:
         area.append(cv2.contourArea(i))
      cv =sum(area)
      # c = max(contours, key=cv2.contourArea)
      # cv = cv2.contourArea(c)


   font = cv2.FONT_HERSHEY_SIMPLEX
   bottomLeftCornerOfText = (0,50)
   fontScale = 2
   fontColor = (0, 255, 0)
   lineType = 2

   cv2.putText(final, str(cv),
               bottomLeftCornerOfText,
               font,
               fontScale,
               fontColor,
               lineType)
   print(cv)

   cv2.imshow("Slicing Tool", final)

   k = cv2.waitKey(1) & 0xFF
   if k == ord('m'):
     mode = not mode
   elif k == 27:
     break

cv2.destroyAllWindows()

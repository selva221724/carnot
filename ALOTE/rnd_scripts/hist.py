import cv2
from skimage.io import imread
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image

im=Image.open("/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/Raw_images/24/Mission_4_1/converted/20200124_12/20200124_123517_R.tif")
pxl=list(im.getdata())
columnsize,rowsize=im.size

a = np.array(pxl)
plt.hist(a, bins = 255)
plt.title("histogram")
plt.show()

# list(set([x for x in a if a.count(x) > 2]))
import cv2

path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/Raw_images/24/Mission_1_1/qgis_converterd_mi1_1'

import os

imgs =[]
for i,j,k in os.walk(path):
    for file in k:
        if '.tif' in file:
            imgs.append(os.path.join(i,file))


for i in imgs:
    img = cv2.imread(i)
    cv2.imwrite(i[:-4]+'.jpg',img)
from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from osgeo import gdal
import simplekml
from pyproj import Proj, transform
import gdal
from skimage.io import imread, imsave
from matplotlib import pyplot as plt

image_path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/tiff/compressd_rdb_alote.tif'
image = imread(image_path)

print('Raster Read Successfully')


# ================================== Sliding Window and Non-Max Suppression ===============================

def sliding_window(image, stepSize, windowSize):
    # slide a window across the image
    for y in range(0, image.shape[0], stepSize):
        for x in range(0, image.shape[1], stepSize):
            # yield the current window
            yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])


srcImage = gdal.Open(image_path)
geoTrans = srcImage.GetGeoTransform()


def set_crs(x, y):
    inProj = Proj(init='epsg:32643')
    outProj = Proj(init='epsg:4326')
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def world2Pixel(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    rtnX = geoMatrix[2]
    rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def KMLgen(features, KML_name, geoTrans):
    world_coord = []
    for i in features:
        temp = []
        for coord in i:
            x, y = Pixel2world(geoTrans, coord[0], coord[1])
            x, y = set_crs(x, y)
            temp.append([x, y])
        world_coord.append(temp)

    # ================================================ Export As a KML ==================================================

    kml = simplekml.Kml()
    for row in world_coord:
        pol = kml.newpolygon(outerboundaryis=[(row[0][0], row[0][1]),
                                              (row[1][0], row[1][1]),
                                              (row[2][0], row[2][1]),
                                              (row[3][0], row[3][1]),
                                              (row[0][0], row[0][1])])
        # pol.description = des
        # pol.style.polystyle.fill = 1
        # pol.style.polystyle.outline = 1
        # pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 255)

    kml.save(KML_name)


def non_max_suppression_fast(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))

    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int")


def tranform_coords(x, y, angle):
    angle_rad = np.deg2rad(angle)
    c, s = np.cos(angle_rad), np.sin(angle_rad)
    rot_matrix = np.array([[c, s], [-s, c]])
    trans = rot_matrix @ [[0, 0, x, y], [0, x, 0, y]]
    x, y = (trans.ptp(axis=1) + 0.5).astype(int)
    return x, y


# ====================================== Image Processing by Each Sliding Window ===============================
print('Started Sliding Window Iterations')

# (winW, winH) = (15000, 15000)
# stepsize=13000


from scipy import ndimage

(winW, winH) = (4648800, 4648800)
stepsize = 4648800

coords = []
for (x, y, window) in sliding_window(image, stepSize=stepsize, windowSize=(winW, winH)):
    ima = window.copy()
    # gray = cv2.cvtColor(window, cv2.COLOR_BGR2GRAY)

    # if not cv2.countNonZero(gray) == 0:
    window = ndimage.rotate(window, -6.5)
    window = cv2.cvtColor(window, cv2.COLOR_RGBA2RGB)
    hsv = cv2.cvtColor(window, cv2.COLOR_RGB2HSV)

    mask = cv2.inRange(hsv, (32, 00, 99), (255, 255, 255))

    # mask = cv2.inRange(hsv, (90, 4, 142), (117, 255, 255))  #second iter

    imask = mask > 0
    green = np.zeros_like(window, np.uint8)
    green[imask] = window[imask]
    gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    kernel = np.ones((2,2), dtype= np.uint8)
    img_dilation = cv2.dilate(threshold, kernel, iterations=1)
    # plt.imshow(img_dilation)

    kernel1 = np.ones((4,4), dtype= np.uint8)
    img_erode = cv2.erode(img_dilation, kernel1, iterations=1)
    # plt.imshow(img_erode)

    # plt.imshow(img_erode)

    _, contours, _ = cv2.findContours(img_erode, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    # area = []
    for i in contours:
        # area.append(cv2.contourArea(i))
        area = cv2.contourArea(i)
        if area > 6000:
            X, Y, w, h = cv2.boundingRect(i)
            # x1 , y1 = tranform_coords(X, Y, +6)
            # x2, y2 = tranform_coords(X,  Y+h, +6)
            # x3, y3 = tranform_coords(X + w, Y + h, +6)
            # x4, y4 = tranform_coords(X+w, Y, +6)
            # coords.append([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])

            buffer = -5

            coords.append([[X + -buffer, Y - buffer], [X - buffer, Y + h + buffer], [X + w + buffer, Y + h + buffer], [X + w + buffer, Y - buffer]])

# plt.figure()
# plt.imshow(window)
#
# list1 = list(zip(*coords[0]))
# plt.scatter(list(list1[0]),list(list1[1]))
# boxes = []
# for i in coords:
#     boxes.append([i[0][0],i[0][1],i[1][0],i[1][1]])
#
#
# boxes = np.array(boxes)
#
thresh = 0.7
# coords_updated = non_max_suppression_fast(boxes,thresh)
#
# coords_final=[]
# for i in coords_updated:
#     coords_final.append([[i[0],i[1]],[i[2],i[3]]])
#
# print(len(coords))
# print(len(coords_final))

KMLgen(coords, '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/tiff/cad.kml', geoTrans)

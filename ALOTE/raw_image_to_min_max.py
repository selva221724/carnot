import cv2
import colorsys

path = '/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/IT_Delivery/images/Mission_1_1/1.jpg'

img = cv2.imread(path)

color = ['#000004', '#02020c', '#050417', '#0a0722', '#10092d', '#160b39', '#1e0c45', '#260c51', '#2f0a5b', '#380962',
         '#400a67', '#490b6a', '#510e6c', '#59106e', '#61136e', '#69166e', '#71196e', '#781c6d', '#801f6c', '#88226a',
         '#902568', '#982766', '#a02a63', '#a82e5f', '#b0315b', '#b73557', '#bf3952', '#c63d4d', '#cc4248', '#d34743',
         '#d94d3d', '#df5337', '#e45a31', '#e9612b', '#ed6925', '#f1711f', '#f47918', '#f78212', '#f98b0b', '#fa9407',
         '#fb9d07', '#fca60c', '#fcb014', '#fbba1f', '#fac42a', '#f8cd37', '#f6d746', '#f4e156', '#f2ea69', '#f2f27d',
         '#f5f992', '#fcffa4']


def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))


def rgb_to_hex(rgb):
    return '%02x%02x%02x' % rgb


color_rgb = [hex_to_rgb(i) for i in color]

# color_hsv  = [colorsys.rgb_to_hsv(i[0],i[1],i[2]) for i in color_rgb]

# img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


h = img.shape[0]
w = img.shape[1]

source_dict ={}

# loop over the image, pixel by pixel
count = 1
for y in range(0, h):
    for x in range(0, w):
        # threshold the pixel
        B, G, R = img[y, x]

        for i in range(len(color_rgb) - 1):
            R1, G1, B1 = color_rgb[i]
            R2, G2, B2 = color_rgb[i + 1]

            if R1 < R < R2:
                if G1 < G < G2:
                    if B1 < B < B2:
                        source_dict.update({count:[R,G,B]})
                        count+=1

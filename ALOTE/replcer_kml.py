import xml.etree.ElementTree as ET
import random
import os

inverter_path ='/mnt/dash/Alpha_Share/AERIAL_THERMOGRAPHY/ALOTE/IT_Delivery/kml_revised'

paths =[]

for i,j,k in os.walk(inverter_path):
    for file in k:
        paths.append(os.path.join(i,file))

def word_replace(filename,old,new):
    c=0
    with open(filename,'r+',encoding ='utf-8') as f:
        a=f.read()
        b=a.split()
        for i in range(0,len(b)):
            if b[i]==old:
                c=c+1
        old=old.center(len(old)+2)
        new=new.center(len(new)+2)
        d=a.replace(old,new,c)
        f.truncate(0)
        f.seek(0)
        f.write(d)
    print('All words have been replaced!!!')



for j in paths:

    # tree = ET.parse(j)
    #
    # for fill in tree.findall("jpg&l"):
    #     # print('Original value of the', color, 'is', fill.text)
    #     fill.text = "jpg'&l"
    #
    # tree.write(j)

    # word_replace(j,"Broken Glass","Multicell Hotspot")

    # Read in the file
    with open(j, 'r') as file:
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace("Broken Glass", "Multicell Hotspot")

    # Write the file out again
    with open(j, 'w') as file:
        file.write(filedata)












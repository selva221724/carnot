# ================================== Import Packages ======================================================

import numpy as np
from osgeo import gdal
import pykml
from pykml import parser
import simplekml
from pyproj import Proj, transform
import gdal
import os
from pathlib import Path

# GB KML FILE
kml_file = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/GIS_TO_Automation/ZoneA_inv_wise_Bound.kml'

OUT_PUT_PATH = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/IT_Delivery/GB_KML'

v = Path(kml_file).name
file = v.replace(".", " ").split()[0]

# ================================== Read KML file ===============================================

f = open(kml_file, "r")
docs = parser.parse(f)
try:

    doc = docs.getroot().Document
    print('Number of features in KML is', len(doc.Placemark))
except:
    try:
        doc = docs.getroot().Document.Folder
        print('Number of features in KML is', len(doc.Placemark))
    except:
        pass
# ===================================== Global Splitting ============================
name =[]
for place in doc.Placemark:
    y = str(place.description)
    # y = y.split('=')
    # v = y.split('<BR>')
    # v = v[2]
    y = y.split(' ')
    y = y[-1]
    name.append(y)


def GB_KML(Name):

    fill =[]
    name = []
    coords =[]
    for place in doc.Placemark:

        y = str(place.description)
        # y = y.split('=')
        # v = y.split('<BR>')
        # v = v[2]
        y = y.split(' ')
        y = y[-1]
        name.append(y)


        x = str(place.Polygon.outerBoundaryIs.LinearRing.coordinates)
        if '\n' in x:
            x = x.replace('\n', ',')
            x = x.replace(' ', '')
            x = x[1:]
        else:
            x = x.replace(' ', ',')
        v = x.split(",")


        line = []
        for i in range(len(v)):
            try:
                if float(v[i]) > 1:
                    line.append(float(v[i]))
            except:
                pass

        cnt = []
        for i in range(0, len(line), 2):
            cnt.append((line[i], line[i + 1]))
        if y== Name:
            fill.append(cnt)
        else:
            coords.append(cnt)



    kml = simplekml.Kml()
    for i in range(len(coords)):
        pol = kml.newpolygon(outerboundaryis=coords[i])
        pol.style.polystyle.fill = 1
        pol.style.polystyle.color = simplekml.Color.rgb(245, 245, 245)
        pol.style.polystyle.outline = 1
        pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 0)

    for i in range(len(fill)):
        pol = kml.newpolygon(outerboundaryis=fill[i])
        pol.style.polystyle.fill = 0
        pol.style.polystyle.outline = 1
        pol.style.linestyle.color = simplekml.Color.rgb(0, 0, 0)


    kml.save(OUT_PUT_PATH+'/'+Name+'/'+'gb.kml')


for i in range(len(name)):
    if not os.path.exists(OUT_PUT_PATH+'/'+name[i]):
        os.makedirs(OUT_PUT_PATH+'/'+name[i])

    GB_KML(name[i])
    print(name[i] +' KML IS EXPORTED')
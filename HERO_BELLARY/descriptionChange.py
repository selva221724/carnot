from pykml import parser
from lxml import etree
import os

defects_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/AERIAL_THERMOGRAPHY/bellary/IT_delivery/kml'

project_path = "'http://183.82.33.43/2k19/HERO_Bellary"

inverters =[]

for i,j,k in os.walk(defects_path):
    for files in j:
        inverters.append(os.path.join(i,files))


# <html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>Panel Number</td><td> 5U</td></tr><tr><td><span style='font-weight:bold'>Defect Type</td><td> Hotspot</td></tr><tr><td><span style='font-weight:bold'>Criticality</td><td> Minor</td></tr><tr><td><span style='font-weight:bold'>Description</td></tr><tr><td colspan='2'><span style='font-weight:bold'> Stressed/Damaged cell. High reflection on a single celll</td></tr><tr><td></td><td></td></tr><tr><td><span style='font-weight:bold'>Latitude</td><td> 13.7417</td></tr><tr><td><span style='font-weight:bold'>Longitude</td><td> 78.6212</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= 'http://106.51.3.224:6660/2k19/hero_thermal/Cropped_Images/ITC_5/ITC_5_4.png'></td></tr></table></body></html>

# < tr class ='w3-green' > < tr > < td > < span style='font-weight:bold' > String Code < / td > < td > 5.1.1.4.2 < / td > < / tr >

def changer(kml_file,output_file,inv_name):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    doc = docs.getroot().Document

    for place in doc.Placemark:
        temp = place.description
        temp = str(temp)
        latlon = str(place.Point.coordinates)
        latlon = latlon.split(',')

        itc = temp.split("ITC No: ")[1].split("\n\nTable No: ")[0]
        tbno = temp.split("Table No: ")[1].split("\n\nDefect: ")[0]
        # tbno_path = tbno.replace('/','ZZ')
        defect = temp.split("Defect: ")[1].split("\n\nDescription: ")[0]
        descrip = temp.split("Description: ")[1]
        lat = latlon[1]
        long = latlon[0]

        b = "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><table class='w3-table-all' border=0 ><tr><td><span style='font-weight:bold'>ITC No:</td><td>"+itc+"</td></tr><tr><td><span style='font-weight:bold'>Table No:</td><td>"+tbno+"</td></tr><tr><td><span style='font-weight:bold'>Defect:</td><td>"+defect+"</td></tr><tr><td><span style='font-weight:bold'>Description:</td><td>"+descrip+"</tr><tr><td><span style='font-weight:bold'>Latitude:</td><td>"+lat+"</td></tr><tr><td><span style='font-weight:bold'>Longitude:</td><td>"+long+"</td></tr><tr><td><span style='font-weight:bold'>Thermal Image</td><td><img src= " + project_path + "/images/"+ inv_name+'/'+defect+'/'+tbno+".jpg'></td></tr></table></body></html>"

        # temp_1 = str(temp)
        place.description._setText(b)

    with open(output_file, "w") as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    with open(output_file, "ab") as f:
        f.write(etree.tostring(docs, pretty_print=True))



for i in inverters:
    from pathlib import Path

    inv_name = Path(i).name
    inv_name = inv_name.split('.')[0]

    path=[]
    for m,n,o in os.walk(i):
        for file in o:
            path.append(os.path.join(m,file))

    for j in path:
        changer(j,j,inv_name)


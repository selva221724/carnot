import geo
from shapely.geometry import Polygon,Point
import os
import math
import numpy as np
import matplotlib.pyplot as plt
os.environ['OPENCV_IO_MAX_IMAGE_PIXELS']=str(2**64)
from datetime import datetime
start_time = datetime.now()
import numpy as np
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path

cv2.useOptimized()

Tifpath = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/Thermal_tiff/inv'

out_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/IT_Delivery/images'

defects_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/GIS_TO_Automation/CAD/Inverter_wise_defects'

inverter_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Nagari/GIS_TO_Automation/CAD/inv_boundary'

inverters = []
for i,j,k in os.walk(inverter_path):
    for file in k:
        inverters.append(os.path.join(i,file))

inverters = sorted(inverters)

tif_paths =[]
for i,j,k in os.walk(Tifpath):
    for file in k:
        if '.tif' in file:
            tif_paths.append(os.path.join(i,file))

tif_paths = sorted(tif_paths)

defects_inverter = []
for i,j,k in os.walk(defects_path):
    for file in k:
        if '.kml' in file:
            defects_inverter.append(os.path.join(i,file))

defects_inverter = sorted(defects_inverter)


class Panel:
    def __init__(self,coords,points):
        self.coords = coords
        self.points = points

class Points:
    def __init__(self,coords,tabelid,defect):
        self.coords = coords
        self.tabelid = tabelid
        self.defect = defect

def w2p(x,y):
    x,y = geo.set_crs(x,y,'4326','32644')
    x,y = geo.world2Pixel(geoTrans,x,y)
    return (x,y)






def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect

def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    # print(M)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


def crop(I,polygon):
    minX = I.shape[1]
    maxX = -1
    minY = I.shape[0]
    maxY = -1
    for point in polygon[0]:

        x = point[0]
        y = point[1]

        if x < minX:
            minX = x
        if x > maxX:
            maxX = x
        if y < minY:
            minY = y
        if y > maxY:
            maxY = y

    # Go over the points in the image if thay are out side of the emclosing rectangle put zero
    # if not check if thay are inside the polygon or not
    cropedImage = np.zeros_like(I)
    for y in range(0, I.shape[0]):
        for x in range(0, I.shape[1]):

            if x < minX or x > maxX or y < minY or y > maxY:
                continue

            if cv2.pointPolygonTest(np.asarray(polygon), (x, y), False) >= 0:
                cropedImage[y, x, 0] = I[y, x, 0]
                cropedImage[y, x, 1] = I[y, x, 1]
                cropedImage[y, x, 2] = I[y, x, 2]

    # Now we can crop again just the envloping rectangle
    finalImage = cropedImage[minY:maxY, minX:maxX]
    return finalImage

def defects_crop(defectKml,cad,tif,out_dir):
    global geoTrans

    tifImg = imread(tif)

    srcImage = gdal.Open(tif)
    geoTrans = srcImage.GetGeoTransform()

    cad_name = Path(cad).name
    cad_name = cad_name.split('.')[0]

    if not os.path.exists(out_dir+'/'+cad_name):
        os.mkdir(out_dir+'/'+cad_name)

    polycoords = geo.kread(cad)

    coords, des = geo.kread(defectKml, kml_type="Point", description=True)

    tabelno = []
    defects = []
    for i in des:
        tbno = i.split("Table No: ")[1].split("\n\nDefect: ")[0]

        tabelno.append(tbno)
        defect = i.split("Defect: ")[1].split("\n\nDescription: ")[0]
        defects.append(defect)

    defectsPts = []
    for (coord,tabelno,defect) in zip(coords,tabelno,defects):
        point = Points(coord,tabelno,defect)
        defectsPts.append(point)

    print('start')
    panels =[]
    for ply in polycoords:
        points = []
        ply = [[float(i) for i in lst] for lst in ply]

        xycoords = []
        for i in ply:
            xycoords.append(w2p(i[0],i[1]))

        # xycoords = [w2p(ply[0][0],ply[0][1]),w2p(ply[1][0],ply[1][1]),w2p(ply[2][0],ply[2][1]),w2p(ply[3][0],ply[3][1])]
        # poly = Polygon(xycoords)
        poly = Polygon(ply)
        for pt in defectsPts:
            # (x,y) = w2p(pt.coords[0], pt.coords[1])
            (x, y) = (pt.coords[0], pt.coords[1])
            if poly.contains(Point(x,y)):
                points.append(pt)
        if len(points) != 0:
                panel = Panel(xycoords,points)
                panels.append(panel)
    print('Itreation stopped')

    count = 0

    print(panels)
    for panel in panels:
        # xlist = []
        # ylist = []
        # for i in panel.coords:
        #     xlist.append(i[0])
        #     ylist.append(i[1])
        # point1 = (int(min(xlist)), int(min(ylist)))
        # point2 = (int(max(xlist)), int(max(ylist)))
        try:
            # coords=[]
            # for k in panel.coords:
            #     coords.append([k[0],k[1]])
            pts = np.array(panel.coords, dtype="float32")
            print(panel.coords)
            crop_img = four_point_transform(tifImg,pts)
            # crop_img = crop(tifImg,coords)

            # crop_img = tifImg[point1[1]-10:point2[1]+10, point1[0]-10:point2[0]+10]

            crop_img = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2RGB)
            points = panel.points
            defects = set()
            for point in points:
                name = point.tabelid
                defects.add(point.defect)

            print(count)
            print("defects:", defects)
            print("name:", name)

            for defect in defects:
                if not os.path.exists(out_dir+'/'+cad_name+'/'+str(defect)):
                    os.mkdir(out_dir+'/'+cad_name+'/'+str(defect))
                imsave(out_dir+'/'+cad_name+'/'+str(defect)+'/'+name + ".jpg", crop_img)

            count += 1
        except Exception as e:
            print(e)



for i in range(len(inverters)):
    defects_crop(defects_inverter[i],inverters[i],tif_paths[i],out_dir)
    print(str(i+1)+' inverter is done')

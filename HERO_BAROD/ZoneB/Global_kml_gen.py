from datetime import datetime
start_time = datetime.now()

# ================================== Import Packages ======================================================
from pykml import parser
import simplekml
import cv2
import os
from pathlib import Path
cv2.useOptimized()

KML_PATH = "/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/Production/IT_Delivery/GROUP"
OUT_PUT_PATH = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/Production/IT_Delivery/GLOBAL'


temp=[]
def KMLgen(feature,des,key,OUT_PUT_PATH):

    # =============================================== Export As a KML ==================================================
    color = {'Open_Circuit':'green', 'Short_Circuit': 'blue', 'Panel_Failure': 'red',
             'By_Pass_Diode_Issues': 'yellow', 'Open_String_Tables': 'orange',
             'Hotspot': 'green', 'Dirt': 'blue', 'Broken_Glass': 'red', 'PID': 'yellow'}

    # print(feature)
    kml = simplekml.Kml()
    for (l,k) in zip(feature,des) :
        pol = kml.newpoint(coords=[l])

        pol.description = k

        pol.style.labelstyle.color = simplekml.Color.red  # Make the text red
        pol.style.labelstyle.scale = 2  # Make the text twice as big

        pol.style.iconstyle.icon.href = 'http://183.82.33.43/GroupL/colour_icon/'+color[key]+'.png'

        kml.save(OUT_PUT_PATH+'/'+key+"_global.kml")




def readkml(kml_file):

    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder

    except:
        doc = docs.getroot().Document

    coo = []
    des=[]
    for place in doc.Placemark:
        y= str(place.Point.coordinates)
        y=y.split(",")
        y = float(y[0]), float(y[1])
        coo.append(y)

        x = str(place.description)
        des.append(x)

    return coo , des

path =[]
for root, directories, filenames in os.walk(KML_PATH):
    for filename in filenames:
        path.append(os.path.join(root,filename))

path = list(sorted(path))

source_dict={}

color = {'Open_Circuit':'green', 'Short_Circuit': 'blue', 'Panel_Failure': 'red',
             'By_Pass_Diode_Issues': 'yellow', 'Open_String_Tables': 'orange',
             'Hotspot': 'green', 'Dirt': 'blue', 'Broken_Glass': 'red', 'PID': 'yellow'}

kml_names = list(color.keys())

for i in kml_names:
    temp=[]
    for j in path:
        kml_name = Path(j).name
        kml_name = kml_name.split('.')[0]
        if kml_name==i:
            temp.append(j)

    source_dict.update({i:temp})



out={}
out_des={}
for (key,value) in source_dict.items():
    temp_data=[]
    temp_des=[]
    for i in value:
        data,des = readkml(i)
        for (j,k) in zip(data,des):

            temp_data.append(j)
            temp_des.append(k)

    out.update({key:temp_data})
    out_des.update({key:temp_des})


for ((k,v),(i,j)) in zip(out.items(),out_des.items()):
    print(k,'in process')
    KMLgen(v,j,k,OUT_PUT_PATH)
from datetime import datetime

start_time = datetime.now()

# ================================== Import Packages ======================================================
import cv2
import numpy as np
from pykml import parser
import os
import pandas as pd

cv2.useOptimized()


defects_kmls_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/Production/IT_Delivery/GROUP'
output_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/Production/IT_Delivery/Counting'

inverter_path = []
inverter_files = []
inverter_names = []
for root, directories, filenames in os.walk(defects_kmls_path):
    for filename in directories:
        inverter_path.append(os.path.join(root, filename))
        inverter_names.append(filename)
        temp = []
        for i, j, k in os.walk(os.path.join(root, filename)):
            for file in k:
                temp.append(os.path.join(i, file))
        inverter_files.append(temp)

inverter_path = sorted(inverter_path)
inverter_files = sorted(inverter_files)
inverter_names = sorted(inverter_names)


def read_points_kml(kml_file):
    f = open(kml_file, "r")
    docs = parser.parse(f)
    try:
        doc = docs.getroot().Document.Folder
    except:
        try:
            doc = docs.getroot().Document
        except:
            pass

    coords = []
    for place in doc.Placemark:
        x = str(place.Point.coordinates)
        x = x.split(",")
        coords.append(list(np.float_(x)))
    return coords


import copy
# ========================================== Create the Dict for Counting =============================
Source_Dict = {}


subclass = {"count": 0}

for i in inverter_names:
    Source_Dict.update(copy.deepcopy({i: copy.deepcopy({"Open Circuit": copy.deepcopy(subclass),
                      "Short Circuit": copy.deepcopy(subclass),
                      "Panel Failure": copy.deepcopy(subclass),
                      "By Pass Diode Issues": copy.deepcopy(subclass),
                    "Open String Tables": copy.deepcopy(subclass),
                    "Hotspot": copy.deepcopy(subclass),
                     "Dirt": copy.deepcopy(subclass),
                     "Broken Glass": copy.deepcopy(subclass),
                        "PID": copy.deepcopy(subclass)})}))


#
# # ========================================= Counting the Classes ======================================

for i, j in zip(inverter_files, inverter_names):

    for defects in i:

        data = read_points_kml(defects)
        Count = len(data)

        if 'Open Circuit' in defects:
            Source_Dict[j]['Open Circuit']['count'] += Count

        elif 'Short Circuit' in defects:
            Source_Dict[j]['Short Circuit']['count'] += Count

        elif 'Panel Failure' in defects:
            Source_Dict[j]['Panel Failure']['count'] += Count

        elif 'By Pass Diode Issues' in defects:
            Source_Dict[j]['By Pass Diode Issues']['count'] += Count

        elif 'Open String Tables' in defects:
            Source_Dict[j]['Open String Tables']['count'] += Count

        elif 'Hotspot' in defects:
            Source_Dict[j]['Hotspot']['count'] += Count

        elif 'Dirt' in defects:
            Source_Dict[j]['Dirt']['count'] += Count

        elif 'Broken Glass' in defects:
            Source_Dict[j]['Broken Glass']['count'] += Count

        elif 'PID' in defects:
            Source_Dict[j]['PID']['count'] += Count
#
# # ======================================== Counting The Total ===========================================
# for keys in Source_Dict:
#     for key in Source_Dict[keys]:
#         Source_Dict[keys][key]['Total'] = Source_Dict[keys][key]['Major'] + Source_Dict[keys][key]['Minor']
#


# # ========================================= Inverter Wise Counting =====================================

color = {'Open Circuit': 'green', 'Short Circuit': 'blue', 'Panel Failure': 'red',
         'By Pass Diode Issues': 'yellow', 'Open String Tables': 'orange',
         'Hotspot': 'green', 'Dirt': 'blue', 'Broken Glass': 'red', 'PID': 'yellow'}



subgroub = list(color.keys())
color_code = list(color.values())

kml_name = ['Open_Circuit',
 'Short_Circuit',
 'Panel_Failure',
 'By_Pass_Diode_Issues',
 'Open_String_Tables',
 'Hotspot',
 'Dirt',
 'Broken_Glass',
 'PID']

length = len(subgroub)

if not os.path.exists(output_dir+'/'+'IT_Deliver'):
    os.mkdir(output_dir+'/'+'IT_Deliver')


summary=[]
count =0
for inv in inverter_names:

    current_value=[]
    for i in Source_Dict[inv]:
        dictList = list(Source_Dict[inv][i].values())
        current_value.append(dictList)

    from itertools import chain
    current_value = list(chain.from_iterable(current_value))
    summary.append(current_value)

    if count==0:
        df = pd.DataFrame(columns=['block_id', 'subgroup','current_value', 'kml_name', 'color_code'], index=range(length))
        df['block_id'] = inv
        df['subgroup'] = subgroub
        df['current_value'] = current_value
        df['kml_name'] = kml_name
        df['color_code'] = color_code
    else:
        dff = pd.DataFrame(columns=['block_id', 'subgroup','current_value', 'kml_name', 'color_code'], index=range(length))
        dff['block_id'] = inv
        dff['subgroup'] = subgroub
        dff['current_value'] = current_value
        dff['kml_name'] = kml_name
        dff['color_code'] = color_code
        df = pd.concat([df, dff])
    count+=1


df.to_csv(output_dir+'/IT_Deliver'+'/'+'Inverter.csv',index=False)


# ============================================= Summary ==========================================================

final_count =[]
for j in range(len(summary[0])):
    temp=[]
    for i in summary:
        temp.append(i[j])
    final_count.append(sum(temp))




df = pd.DataFrame(columns=['defect_type', 'Count','kml', 'color'], index=range(length))
df['defect_type'] = subgroub
df['Count'] = final_count
df['kml'] = kml_name
df['color'] = color_code
df.to_csv(output_dir+'/IT_Deliver'+'/'+'Summary.csv',index=False)
import geo
from shapely.geometry import Polygon,Point
from datetime import datetime
start_time = datetime.now()
# ================================== Import Packages ======================================================
import cv2
import gdal
from skimage.io import imread,imsave
import os
from pathlib import Path

cv2.useOptimized()

Tifpath = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/Thermal_Tiff/revised/Zone_A_final.tif'

out_dir = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/IT_Delivery/Images'

defects_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/GIS_TO_Automation/KML'

inverter_path = '/mnt/7dd94a34-2d42-4a73-815a-8021eb0a573d/Alpha_Share/Mitarsh_AT/30MW/GIS_TO_Automation/INVERTER_WISE_CAD'

inverters = []
for i,j,k in os.walk(inverter_path):
    for file in k:
        inverters.append(os.path.join(i,file))

inverters = sorted(inverters)



defects_inverter = []
for i,j,k in os.walk(defects_path):
    for file in k:
        if '.kml' in file:
            defects_inverter.append(os.path.join(i,file))

defects_inverter = sorted(defects_inverter)


class Panel:
    def __init__(self,coords,points):
        self.coords = coords
        self.points = points

class Points:
    def __init__(self,coords,tabelid,defect):
        self.coords = coords
        self.tabelid = tabelid
        self.defect = defect

def w2p(x,y):
    x,y = geo.set_crs(x,y,'4326','32643')
    x,y = geo.world2Pixel(geoTrans,x,y)
    return (x,y)



srcImage = gdal.Open(Tifpath)
geoTrans = srcImage.GetGeoTransform()






def defects_crop(defectKml,cad,Tifpath,out_dir):

    cad_name = Path(cad).name
    cad_name = cad_name.split('.')[0]

    if not os.path.exists(out_dir+'/'+cad_name):
        os.mkdir(out_dir+'/'+cad_name)

    polycoords = geo.kread(cad)

    coords, des = geo.kread(defectKml, kml_type="Point", description=True)

    tabelno = []
    defects = []
    for i in des:
        tbno = i.split("Table No: ")[1].split("\n\nDefect: ")[0]

        tabelno.append(tbno)
        defect = i.split("Defect: ")[1].split("\n\nDescription: ")[0]
        defects.append(defect)

    defectsPts = []
    for (coord,tabelno,defect) in zip(coords,tabelno,defects):
        point = Points(coord,tabelno,defect)
        defectsPts.append(point)

    print('start')
    panels =[]
    for ply in polycoords:
        points = []
        ply = [[float(i) for i in lst] for lst in ply]

        xycoords = []
        for i in ply:
            xycoords.append(w2p(i[0],i[1]))

        # xycoords = [w2p(ply[0][0],ply[0][1]),w2p(ply[1][0],ply[1][1]),w2p(ply[2][0],ply[2][1]),w2p(ply[3][0],ply[3][1])]
        # poly = Polygon(xycoords)
        poly = Polygon(ply)
        for pt in defectsPts:
            # (x,y) = w2p(pt.coords[0], pt.coords[1])
            (x, y) = (pt.coords[0], pt.coords[1])
            if poly.contains(Point(x,y)):
                points.append(pt)
        if len(points) != 0:
                panel = Panel(xycoords,points)
                panels.append(panel)
    print('Itreation stopped')

    count = 0

    tifImg = imread(Tifpath)
    for panel in panels:
        xlist = []
        ylist = []
        for i in panel.coords:
            xlist.append(i[0])
            ylist.append(i[1])
        point1 = (int(min(xlist)), int(min(ylist)))
        point2 = (int(max(xlist)), int(max(ylist)))

        crop_img = tifImg[point1[1]-10:point2[1]+10, point1[0]-10:point2[0]+10]

        crop_img = cv2.cvtColor(crop_img, cv2.COLOR_RGBA2RGB)
        points = panel.points
        defects = set()
        for point in points:
            name = point.tabelid
            defects.add(point.defect)

        print(count)
        print("defects:", defects)
        print("name:", name)

        for defect in defects:
            if not os.path.exists(out_dir+'/'+cad_name+'/'+str(defect)):
                os.mkdir(out_dir+'/'+cad_name+'/'+str(defect))
            imsave(out_dir+'/'+cad_name+'/'+str(defect)+'/'+name + ".jpg", crop_img)

        count += 1


for i in range(len(inverters)):
    defects_crop(defects_inverter[i],inverters[i],Tifpath,out_dir)
    print(str(i+1)+' inverter is done')
